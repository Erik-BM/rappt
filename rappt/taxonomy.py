# species api

from rdflib import Graph, Literal, Namespace, URIRef, BNode
from rdflib.namespace import FOAF, RDF, OWL, RDFS
from rdflib.plugins.sparql import prepareQuery
from collections import defaultdict
from SPARQLWrapper import SPARQLWrapper, JSON, TURTLE
from .utils import prefixes, read_data, test_endpoint, query_endpoint, query_graph, length_file, deprecated
import warnings
from .utils import strip as strip_
from fuzzywuzzy import fuzz
from tqdm import tqdm
import pandas as pd
import validators

import glob
import itertools

nan_values = ['nan', float('nan'),'--','-X','NA','NC',-1,'','sp.', -1,'sp,','var.','variant','NR','no rank']

FORMAT = 'nt'

def wikidata_common_name(i):
    endpoint = 'https://query.wikidata.org/sparql'
    q = """
    SELECT ?name WHERE {
    SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". }
    ?item wdt:P685 "%s" . 
    ?item wdt:P225 ?name .
    }
    """ % str(i)
    try:
        res = query_endpoint(endpoint, q, var = ['name'])
    except:
        return []
    
    out = []
    for name in res:
        out.append(name)
    return out


def ncbi_to_eol_mapping():
    endpoint = 'https://query.wikidata.org/sparql'
    q = """
    SELECT ?ncbi ?eol WHERE {
    ?a wdt:P830 ?eol ;
        wdt:P685 ?ncbi .
    }
    """
    try:
        res = query_endpoint(endpoint, q, var = ['ncbi','eol'])
    except:
        return []
    
    out = {}
    for a,b in res:
        out[a] = b
    return out

def load_ncbi_to_eol_from_file(filename):
    out = {}
    d = pd.read_csv(filename, sep=',', low_memory = False)
    for a,b in zip(d['ncbi'],d['eol']):
        out[a] = b
    
    return out

def strip_compair(name,n):
    return fuzz.ratio(name.lower(), n.lower())/100

class Taxonomy:
    """
    Read and manipulate the NCBI taxonomy.
    """
    def __init__(self, directory = '', eol_directory = '', endpoint = '', rdf_file = '', namespace = 'http://example.org/ncbi', verbose = False, testing = False):
        """
        args:
            directory :: str 
                optional, path to files
            endpoint :: str 
                optional, host for sparql endpoint.
            namespace :: str
                default namespace to use for rdf data.
        """
        try:
            assert directory or endpoint
        except AssertionError:
            raise ValueError ('Neither directory or endpoint was provided.')
        finally:
            if directory:
                if verbose: print('Loading data from directory: ' + directory)
            elif endpoint:
                if verbose: print('Loading data from endpoint: ' + endpoint)            
        
        if not test_endpoint(endpoint):
            self.endpoint = ''
        
        self.directory = directory
        self.verbose = verbose
        self.testing = testing
        
        self.hierarchy_file = directory + 'nodes.dmp'
        self.names_file = directory + 'names.dmp'
        self.division_file = directory + 'division.dmp'
        
        self.eol_directory = eol_directory
        
        self.namespace = Namespace(namespace+'/')
        self.onto_namespace = Namespace(namespace+'#')
        self.endpoint = endpoint
        self.initNs = {'rdf':RDF, 'foaf':FOAF, 'ns':self.namespace, 'nso':self.onto_namespace, 'owl':OWL, 'rdfs':RDFS}
        
        if not self.endpoint:
            self.graph = Graph()
            if rdf_file:
                self.graph.parse(rdf_file, format=FORMAT)
            else:
                self._load_hierarchy()
                self._load_division()
                self._load_names()
                
                if self.eol_directory:
                    self._load_traits(eol_directory+'trait_bank/traits.csv')
                    for f in glob.glob(eol_directory+'eol_rels/*.csv'):
                        self._load_eol_subclasses(f)
                    
        if not self.endpoint:
            for k,i in self.initNs.items():
                self.graph.bind(k,i)
        
        
    def __dict__(self):
        return {
                'namespace':self.namespace,
                'directory': self.directory,
                'endpoint': self.endpoint,
                'namespaces': self.initNs
            }
        
    ### DATA LOADING FUNCTIONS
        
    def _load_hierarchy(self):
        # columns in nodes.dmp file of Taxonomy. Only use the first three columns (currently).
        data = read_data(self.hierarchy_file)
        
        if self.verbose:
            pbar = tqdm(total=length_file(self.hierarchy_file), desc='Loading hierarchy')
        else:
            pbar = None
        j = 0
        for d in data:
            j += 1
            if self.testing and j > 100:
                break
            # id, parent, rank, division
            i,p,r,di = d[0], d[1], d[2], d[4]
            i = self.namespace['taxon/'+str(i)]
            self.graph.add((i, RDF.type, self.onto_namespace['Taxon']))
            if r and not r in nan_values:
                rt = r.replace(' ','_')
                self.graph.add((i, self.onto_namespace['rank'], self.namespace['rank/'+rt]))
                self.graph.add((self.namespace['rank/'+rt], RDFS.label, Literal(r)))
                self.graph.add((self.namespace['rank/'+rt], RDF.type, self.onto_namespace['Rank']))
            if p and not p in nan_values:
                p = self.namespace['taxon/'+str(p)]
                self.graph.add((i, RDFS.subClassOf, p))
            if di and not di in nan_values:
                di = self.namespace['division/'+str(di)]
                self.graph.add((i, RDFS.subClassOf, di))
            
            if pbar: 
                pbar.update(1)
                
    
    def _load_names(self):
        data = read_data(self.names_file)
        if self.verbose:
            pbar = tqdm(total=length_file(self.names_file), desc='Loading names')
        else:
            pbar = None
            
        i = 0
        for d in data:
            i += 1
            if self.testing and i > 100:
                break
            id_, name, unique_name, type_ = d[0], d[1], d[2], d[3]
            if id_ in nan_values: continue
        
            id_ = self.namespace['taxon/'+str(id_)]
            if not name in nan_values:
                name = Literal(name)
            if not unique_name in nan_values:
                unique_name = Literal(unique_name)
                self.graph.add( (id_, RDFS.label, unique_name) )
            if not type_ in nan_values:
                type_label = Literal(type_)
                type_ = self.onto_namespace[type_.replace(' ','_')]
                self.graph.add( (id_, type_, name) )
                self.graph.add( (type_, RDFS.label, type_label) )
            
            if pbar:
                pbar.update(1)
    
    def _load_division(self):
        data = read_data(self.division_file)
        all_divisions = set()
        j = 0
        for d in data:
            j += 1
            if self.testing and j > 100:
                break
            # id, acronym, full
            id_,k,i = d[0], d[1], d[2]
            if id_ in nan_values: continue
            id_ = URIRef(self.namespace['division/'+ str(id_)])
            if not i in nan_values:
                self.graph.add((id_, RDFS.label, Literal(i)))
            
            self.graph.add((id_, RDF.type, self.onto_namespace['Division']))
            self.graph.add((id_, RDF.type, OWL.Class))
                
            all_divisions.add(id_)
            
    def _load_ssd(self):
        data = read_data(self.ssd_file)
        j = 0
        for d in data:
            j += 1
            if self.testing and j > 100:
                break
            # id, ssd group
            i, s = d[0], d[1]
            i = self.namespace['taxon/'+str(i)]
            s = self.namespace['SSD/'+str(i)]
            self.graph.add((i, RDF.type, s))
            self.graph.add((s, RDF.type, OWL.Class))
            self.graph.add((s, RDF.type, self.onto_namespace['SSDClass']))
       
    def _load_traits(self, filename):
        chunksize = 10**3
        
        data = pd.read_csv(filename, sep=',', usecols=['page_id','predicate','value_uri'], low_memory=False, chunksize=chunksize)
        
        if not hasattr(self,'ncbi_to_eol'):
            self.ncbi_to_eol = ncbi_to_eol_mapping()
        
        eol_to_ncbi = {i:k for k,i in self.ncbi_to_eol.items()}
        
        if self.verbose:
            tot = sum(1 for row in open(filename, 'r'))//chunksize
            pbar = tqdm(total = tot, desc='Loading Traits')
        else:
            pbar = None
        
        i = 0
        for chunk in data:
            if pbar: pbar.update(1)
            for s,p,o in zip(chunk['page_id'],chunk['predicate'],chunk['value_uri']):
                
                i += 1
                if self.testing and i > 100:
                    break
                
                s = str(s)
                try:
                    s = self.namespace['taxon/'+str(eol_to_ncbi[s])]
                    s = URIRef(s)
                except KeyError:
                    pass
                
                s = URIRef(s)
                p = URIRef(p)
                
                try:
                    o = URIRef(o)
                    val = validators.url(o)
                except TypeError:
                    o = Literal(o)
                    val = True
                    
                if validators.url(s) and validators.url(p) and val:
                    self.graph.add((s,p,o))
    
            
    def _load_eol_subclasses(self, filename):
        
        try: 
            try:
                data = pd.read_csv(filename,sep=',',usecols=['child','parent'],low_memory=False)
            except ValueError:
                data = pd.read_csv(filename,sep=',',header=None,low_memory=False)
                data.columns = ['parent','child']
            
            i = 0
            
            for c,p in zip(data['child'],data['parent']):
                i += 1
                if self.testing and i > 100: break
                if validators.url(c) and validators.url(p):
                    c,p = URIRef(c),URIRef(p)
                    self.graph.add((c,RDFS.subClassOf,p))
        except FileNotFoundError as e:
            print(e,filename)
               
        
    ### PRIVATE FUNCTIONS
    def _query(self, q, var):
        if self.endpoint:
            results = query_endpoint(self.endpoint, q, var = var)
        else:
            results = query_graph(self.graph, q)
        
        if self.endpoint and len(result) == 10000:
            # Unique to virtuoso?
            warnings.warn('Only 10k results will be loaded from endpoint.')
        
        if len(var) < 2:
            results = [r[0] for r in results]
        
        return results
    
    def _species(self):
        # return all species
        q = prefixes(self.initNs)
        q += """
            SELECT DISTINCT ?c WHERE {
                ?c rdf:type nso:Species .
            }
        """
        self._query(q, var = ['c'])          
            
        out = set()
        for row in results:
            out.add(row)
        return out
    
    def _names(self, subset = None, name_type = None):
        # return names of species.
        q = prefixes(self.initNs)
        if subset:
            q += """
                SELECT ?c ?n ?t ?un {
                    values ?c { %s }
                    ?c rdf:type nso:Taxon ;
                    OPTIONAL { ?c rdfs:label ?un }
                    OPTIONAL { ?c ?nt ?n .
                               ?nt rdfs:label ?t ;
                                   rdfs:subPropertyOf rdfs:label .
                    }
                }
            """ % '{0}'.format(' '.join(['<'+str(s)+'>' for s in subset]))
        else:
            q += """
            SELECT ?c ?n ?t ?un WHERE {
                ?c rdf:type nso:Taxon ;
                    OPTIONAL { ?c rdfs:label ?un }
                    OPTIONAL { ?c ?nt ?n .
                               ?nt rdfs:label ?t ;
                                   rdfs:subPropertyOf rdfs:label .
                    }
            }
        """
        results = self._query(q, var = ['c', 'n', 't', 'un'])
        out = defaultdict(list)
        
        for s,n,t,un in results:
            if name_type:
                if t in name_type:
                    out[s].append({t:n})
                else:
                    continue
            else:
                out[s].append({t:n})
                
        for s in out:
            d = defaultdict(list)
            for dict_ in out[s]:
                for k,i in dict_.items():
                    d[k].append(i)
            out[s] = d
        
        return out
    
    def _construct_tree(self, subset = None):
        # return set of (child,parent) pairs.
        q = prefixes(self.initNs)
        
        sparql = SPARQLWrapper(self.endpoint)
        if subset:
            q += """
                SELECT ?c ?p {
                    values ?c { %s }
                     ?c rdfs:subClassOf ?p ;
                        a nso:Taxon .
                }
            """ % '{0}'.format(' '.join(['<'+str(s)+'>' for s in subset]))
        else:
            q += """
                SELECT ?c ?p WHERE {
                    ?c rdfs:subClassOf ?p ;
                        a nso:Taxon .
                }
            """
        
        result = self._query(q, var = ['c','p'])
        out = []
        for c,p in result:
            out.append({'child':c, 'parent':p, 'score':1})
        
        return out
    
    def _sibling(self, cid):
        q = prefixes(self.initNs)
        q +=  """
            SELECT ?p {
                VALUES ?s {<%s>}
                    {   ?s rdfs:subClassOf [
                        ^rdfs:subClassOf ?p
                        ]   .  
                        FILTER ?s != ?p
                    }
                    UNION 
                    {   ?s rdf:type [
                        ^rdf:type ?p
                        ]   . 
                        FILTER ?s != ?p
                    }
                    
                }
            """ % cid
        
        result = self._query(q, var = ['p'])
        
        out = set()
        for row in result:
            out.add(row)
        return out
    
    def _siblings(self, species):
        return {s:self._sibling(s) for s in species}
    
    def _traverse_help(self, l, sb, species, down):
        out = set()
        s = ''
        for k in range(l + 1):
            if k == l:
                s += sb
            else:
                s += sb+'/'
        q = prefixes(self.initNs)
        a = '{0}'.format(' '.join(['<'+str(s)+'>' for s in species]))
        if down:
            q += """SELECT DISTINCT ?s {
                values ?x { %s }
                { ?s %s ?x }
                }"""  % (a,s)
        else:
            q += """SELECT DISTINCT ?s {
                values ?x { %s }
                { ?x %s ?s }
                }"""  % (a,s)
        
        
        result = self._query(q, var = ['s'])
        
        for r in result:
            out.add(r)
        return out
    
    def _traverse(self, species, levels = 1, down = False):
        if not species:
            return self._species()
        
        out = set()
        a = ''
        if down:
            a = '^'
        
        initNs = {'ns':self.namespace}
        q = prefixes(initNs)
        
        out = set()
        l = 0
        
        length = len(species)
        
        if levels == -1: # to the bottom/top
            return self._traverse(species, levels = float('inf'), down = down)
            
        else: 
            while l < levels:
                out |= self._traverse_help(l, 'rdfs:subClassOf', species, down)
                if len(out) <= length:
                    break
                length = len(out)
                l += 1
            return out
    
    def _subset(self, species, up_levels = 1, down_levels = 1, convert = False, strip = False):
        if convert: species = [self.namespace['id#'+str(s)] for s in species]
        out = self._traverse(species, up_levels, down = False) | self._traverse(species, down_levels, down = True) | set(species)
        if strip: out = set([strip(s, '#') for s in out])
        return out
    
    def _save(self, directory, f = FORMAT):
        if self.endpoint:
            raise NotImplementedError ('save from endpoint is not implemented.')
        else:
            self.graph.serialize(directory + 'taxonomy.' + f, format = f)
            
    def _classes(self):
        q = prefixes(self.initNs)
        q += """
            SELECT ?c ?n ?l WHERE {
                ?c a owl:Class .
                OPTIONAL {?c rdfs:label ?l}
            }
        """ 
        res = self._query(q, var = ['c','n','l'])
        return res 
    
    def _ssds(self):
        q = prefixes(self.initNs)
        q += """
            SELECT ?c ?n ?l WHERE {
                ?c a owl:Class ;
                    a nso:SSDClass .
                OPTIONAL {?c rdfs:label ?l}
            }
            ORDER BY ?c
        """
        return self._query(q, var = ['c','n','l'])
        
    def _divisions(self):
        q = prefixes(self.initNs)
        q += """
            SELECT ?c ?n ?l WHERE {
                ?c a owl:Class ;
                    a nso:Division .
                OPTIONAL {?c rdfs:label ?l}
            }
            ORDER BY ?c
        """
        return self._query(q, var = ['c','n','l'])
    
    
    def _class_single(self, class_uri):
        out = []
        res = []
        i = 1
        while res or i == 1:
            q = prefixes(self.initNs)
            
            if i == 1:
                s1 = 'rdfs:subClassOf'
                s2 = 'rdf:type'
            else:
                s1 = '/'.join(['rdfs:subClassOf'] * i)
                s2 = '/'.join(['rdf:type'] * i)
            
            q += """
            SELECT ?s {
                VALUES ?c {<%s>}
                ?s rdf:type nso:Species .
                    { ?s %s ?c . }
                    UNION 
                    { ?s %s ?c . }
                }
            """ % (str(class_uri), s1, s2)
            res = self._query(q, var = ['s'])
            out.extend(res)
            i += 1
        return out
    
    def _class_(self, class_ids):
        return {c:self._class_single(c) for c in class_ids}
    
    def _levels(self):
        q = prefixes(self.initNs)
        q += """
            SELECT DISTINCT ?r ?l WHERE {
                ?r rdf:type nso:Rank 
                OPTIONAL { ?r rdfs:label ?l}
            }
        """
        out = self._query(q, ['r', 'l'])
        return out
        
    def _ids(self, name, rank):
        q = prefixes(self.initNs)
        levels = [a for a,_ in self._levels()]
        use_rank = any([str(rank) == str(a) for a in levels])
        rank = str(rank)
        if use_rank:
            q += """
                SELECT ?s ?n WHERE {
                    ?s rdf:type nso:Taxon ;
                            rdf:type <%s> ;
                            rdfs:label ?n .
                        OPTIONAL {?s ?nt ?n .
                                ?nt rdfs:subPropertyOf rdfs:label .}
                    FILTER(regex(STR(?n), "%s", 'i'))
                }
            """ % (rank, name)
        else:
            q += """
                SELECT ?s ?n WHERE {
                    ?s rdf:type nso:Taxon ;
                            rdf:type <%s> ;
                            rdfs:label ?n .
                        OPTIONAL {?s ?nt ?n .
                                ?nt rdfs:subPropertyOf rdfs:label .}
                    FILTER(regex(STR(?n), "%s", 'i'))
                }
            """ % name
        out = list(self._query(q, ['s','n']))
        return out
    
    def _ids_any(self, name, rank):
        offset = 0
        res = []
        out = []
        levels = [a for a,_ in self._levels()]
        use_rank = any([str(rank) == str(a) for a in levels])
        rank = str(rank)
        while res or offset == 0:
            q = prefixes(self.initNs)
            if use_rank:
                q += """
                    SELECT ?s ?n WHERE {
                        ?s rdf:type nso:Taxon ;
                            rdf:type <%s> ;
                            rdfs:label ?n .
                        OPTIONAL {?s ?nt ?n .
                                ?nt rdfs:subPropertyOf rdfs:label .}
                    } 
                    ORDER BY ?s
                    OFFSET %s
                """ % (rank, str(offset))
            else:
                q += """
                    SELECT ?s ?n WHERE {
                        ?s a nso:Taxon ;
                        OPTIONAL {?s ?nt ?n .
                                ?nt rdfs:subPropertyOf rdfs:label .}
                    }
                    ORDER BY ?s
                    OFFSET %s
                """ % str(offset)
            res = list(self._query(q, ['s','n']))
            out.extend(res)
            offset += 10000
        return out
    
    def _habitat(self, s):
        q = prefixes(self.initNs)
        q += """
            SELECT ?h WHERE {
                <%s> <http://rs.tdwg.org/dwc/terms/habitat> ?h .
            }
        """ % s
        res = list(self._query(q, ['h']))
        return res
    
    def _ecoregion(self, s):
        q = prefixes(self.initNs)
        q += """
            SELECT ?h WHERE {
                <%s> <https://www.wikidata.org/entity/Q295469> ?h .
            }
        """ % s
        res = list(self._query(q, ['h']))
        return res
    
    def _endemic(self, s):
        q = prefixes(self.initNs)
        q += """
            SELECT ?h WHERE {
                <%s> <http://eol.org/terms/endemic> ?h .
            }
        """ % s
        res = list(self._query(q, ['h']))
        return res
    
    def _extinct_status(self, s):
        q = prefixes(self.initNs)
        q += """
            SELECT ?h WHERE {
                <%s> <http://eol.org/schema/terms/ExtinctionStatus> ?h .
            }
        """ % s
        res = list(self._query(q, ['h']))
        return res
    
    def _conservation_status(self, s):
        q = prefixes(self.initNs)
        q += """
            SELECT ?h WHERE {
                <%s> <http://rs.tdwg.org/ontology/voc/SPMInfoItems#ConservationStatus> ?h .
            }
        """ % s
        res = list(self._query(q, ['h']))
        return res
    
    def _eol_data(self,function,species,convert,strip):
        
        if convert:
            species = [self.namespace['taxon/'+str(s)] for s in species]
            
        out = defaultdict(list)
        for s in species:
            h = function(s)
            out[s].append(s)
            
        if strip:
            out = {strip_(s):out[s] for s in out}
        
        return out
        
    ### PUBLIC FUNCTIONS
    
    def divisions(self, strip = False):
        """
        Get division domains.
        """
        res = self._divisions()
        out = []
        for c,n,l in res:
            if strip: c = strip_(c,['/','#'])
            out.append({'id':c,'name':n,'label':l})
        return out
    
    def habitat(self, species, convert = False, strip = False):
        """
        Get habitats of species.
        Args:
            species :: list
                list of ncbi ids, if convert
                with namespace, if not convert.
        """
        return self._eol_data(self._habitat,species,convert,strip)
    
    def endemic(self, species, convert = False, strip = False):
        """
        Get endemic region of species.
        Args:
            species :: list
                list of ncbi ids, if convert
                with namespace, if not convert.
        """
        return self._eol_data(self._endemic,species,convert,strip)

    def ecoregion(self, species, convert = False, strip = False):
        """
        Get ecoregion of species.
        Args:
            species :: list
                list of ncbi ids, if convert
                with namespace, if not convert.
        """
        return self._eol_data(self._ecoregion,species,convert,strip)
    
    def conservation_status(self, species, convert = False, strip = False):
        """
        Get conservation status of species.
        Args:
            species :: list
                list of ncbi ids, if convert
                with namespace, if not convert.
        """
        return self._eol_data(self._conservation_status,species,convert,strip)
    
    def extinct_status(self, species, convert = False, strip = False):
        """
        Get extinct status of species.
        Args:
            species :: list
                list of ncbi ids, if convert
                with namespace, if not convert.
        """
        return self._eol_data(self._extinct_status,species,convert,strip)

    def ids(self, name, rank = None, convert = False, strip = False, top_n = -1):
        """
        Get ids of name. Will score full partial matches.
        Args:
            name :: string
            rank :: string
                optional, taxonomic level to search
            convert :: boolean
                add namespace to input rank
            strip :: boolean
                remove namespace from output
        Returns:
            out :: list 
                list of ids that matched
        """
        if convert and rank: rank = self.namespace['rank/'+rank]
        out = self._ids(name, rank = rank)
        out = [{'id':i, 'name':n, 'score':strip_compair(name,n)} for i,n in out]
        if strip: out = [{k:strip_(d[k],['/','#']) for k in d} for d in out]
        out = sorted(out, key = lambda d: d['score'], reverse=True)
        return out[:top_n]
    
    def ids_any(self, name, rank = None, convert = False, strip = False, top_n = -1):
        """
        Get ids of name. Will score any match.
        Args:
            name :: string
            rank :: string
                optional, taxonomic level to search
            convert :: boolean
                add namespace to input rank
            strip :: boolean
                remove namespace from output
        Returns:
            out :: list 
                list of ids that matched
        """
        out = self.ids(name = name, rank = rank, convert = convert, strip = strip, top_n = top_n)
        if out:
            return out
        if convert and rank: rank = self.namespace['rank/'+rank]
        out = self._ids_any(name, rank = rank)
        out = [{'id':i, 'name':n, 'score':strip_compair(name,n)} for i,n in out]
        if strip: out = [{k:strip_(d[k],['/','#']) for k in d} for d in out]
        out = sorted(out, key = lambda d: d['score'], reverse=True)
        return out[:top_n]
    
    
    def levels(self, strip = False):
        """
        Return levels names and id.
        Args:
            strip :: boolean
                strip namespace of output
        Returns:
            out :: list
                list of levels
        """
        res = self._levels()
        out = []
        for r,l in res:
            if strip: r = strip_(r, ['/','#'])
            out.append({'id':r, 'label':l})
        return out
    
    def classes(self, strip = False):
        """
        Returns all classes in dataset.
        """
        out = []
        res = self._classes()
        for c,n,l in res:
            if strip: c = strip_(c,['/','#'])
            out.append({'id':c,'name':n,'label':l})
        return out
    
    def class_name(self, c):
        """
        Return class name.
        Args: 
            c :: str
                class URI
        """
        return self._class_name(c)
    
    def ssds(self, strip = False):
        """
        Returns all ssds.
        """
        out = []
        res = self._ssds()
        for c,n,l in res:
            if strip: c = strip_(c,['/','#'])
            out.append({'id':c,'name':n,'label':l})
        return out
    
    def save(self, directory = './', f = FORMAT):
        """
        Save data to RDF.
        Args:
            directory :: str 
                path to save files at.
        """
        self._save(directory, f)
        
    def names(self, subset = None, convert = False, strip = False):
        """
        Get names and synonyms for species.
        Args:
            subset :: set
                set of species 
            convert :: boolean
                whether to add namespace to subset. 
            strip :: boolean
                whether to strip namespace on output.
        Returns:
            out :: dict 
                key - species id
                item - list of names
        """
        if convert: subset = [self.namespace['taxon/'+str(s)] for s in subset]
        out = self._names(subset)
        if strip: out = {strip_(k,['/','#']):out[k] for k in out}
        return out
        
    def species(self, strip = False):
        """
        All species.
        Args:
            strip :: boolean
                remove namespace from output
        Returns:
            out :: list
                all species in dataset.
        """
        out = self._species()
        if strip: out = set([strip_(s,['/','#']) for s in out])
        return out
    
    def construct_tree(self, subset = None, convert = False, strip = False):
        """
        Construct hierarchy.
        Args:
            subset :: set
                set of species 
            convert :: boolean
                whether to add namespace to subset. 
            strip :: boolean
                whether to strip namespace on output.
        Returns:
            out :: list of dicts
                keys - (child, parent, score)
                items - (id, id, adjacency) 
        """
        if convert: subset = [self.namespace['taxon/'+str(s)] for s in subset]
        out = self._construct_tree(subset)
        if strip: out = [{k:strip_(s[k],['#','/']) for k in s} for s in out]
        return out
    
    def siblings(self, species, convert = False, strip = False):
        """
        Construct hierarchy.
        Args:
            species :: set
                set of species 
            convert :: boolean
                whether to add namespace to subset. 
            strip :: boolean
                whether to strip namespace on output.
        Returns:
            out :: list of dicts
                key - species
                items - list of siblings 
        """
        if convert: species = [self.namespace['taxon/'+str(s)] for s in species]
        out = self._siblings(species)
        if strip: out = {strip_(s, ['/','#']):[strip_(k,['/','#']) for k in out[s]] for s in out}
        return out
    
    def subset(self, species, up_levels = 1, down_levels = 1, convert = False, strip = False):
        """
        Find all species or classes during a traverse of tree.
        Args:
            species :: set
                set of species
            up_levels :: int
                number of levels to traverse upward. -1 denotes to the top.
            down_levels :: int
                number of levels to traverse downward. -1 denotes to the bottom.
            convert :: boolean
                whether to add namespace to subset. 
            strip :: boolean
                whether to strip namespace on output.
        Returns:
            tmp1 :: set
                set of all species passed during traverse.
        """
        if convert: species = [self.namespace['taxon/'+str(s)] for s in species]
        tmp1 = self._subset(species, up_levels, down_levels)
        tmp2 = self.siblings(tmp1)
        for k in tmp2:
            tmp1 |= tmp2[k]
        if strip: tmp1 = set([strip_(s,['/','#']) for s in tmp1])
        return tmp1
    
    def class_(self, class_ids, convert = False, strip = False):
        """
        Return all species in a class.
        Args:
            class_ids :: list
                ids or URI for class
            convert :: boolean
                whether to add namespace
            strip :: boolean
                whether to strip namespace
        Returns:
            out :: dict
                key - class_id
                item - list of species ids
        """
        
        if convert: class_ids = [self.namespace[str(s)] for s in class_ids]
        out = self._class_(class_ids)
        if strip: out = {strip_(k,['#','/']):[strip_(s,['/','#']) for s in out[k]] for k in out}
        return out
    
    def division(self, class_ids, convert = False, strip = False):
        """
        Return all species in a division.
        Args:
            class_ids :: list
                ids or URI for class
            convert :: boolean
                whether to add namespace
            strip :: boolean
                whether to strip namespace
        Returns:
            out :: dict
                key - class_id
                item - list of species ids
        """
        
        if convert: class_ids = [self.namespace['division/'+str(s)] for s in class_ids]
        out = self._class_(class_ids)
        if strip: out = {strip_(k,['#','/']):[strip_(s,['/','#']) for s in out[k]] for k in out}
        return out
    
    def ssd(self, class_ids, convert = False, strip = False):
        """
        Return all species in a ssd group.
        Args:
            class_ids :: list
                ids or URI for class
            convert :: boolean
                whether to add namespace
            strip :: boolean
                whether to strip namespace
        Returns:
            out :: dict
                key - class_id
                item - list of species ids
        """
        
        if convert: class_ids = [self.namespace['ssd/'+str(s)] for s in class_ids]
        out = self._class_(class_ids)
        if strip: out = {strip_(k,['#','/']):[strip_(s,['/','#']) for s in out[k]] for k in out}
        return out
    
    def query(self, q, var):
        """
        Return result of a SPARQL query.
        Args:
            q :: string
                query
            var :: list
                variables in query
                
        Returns:
            res :: list
                list of tuples of variables
        """
        q = prefixes(self.initNs) + q
        return self._query(q,var)
    
    
    
