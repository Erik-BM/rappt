
from .keys import load_dict, get_units, get_types
from collections import defaultdict

import pandas as pd
import numpy as np

nan_values = ['NaN','nan','No information/Not reported','No information']


def normalize(data_units, units):
    v = []
    for a in data_units:
        # larges match in for unit
        b = sorted([key for key in units if key in a], key=lambda a: len(a), reverse = True)[0]
        v.append(units[b])
    return v

class CaseStudy:
    def __init__(self, filename, units_file = None):
        self.filename = filename
        
        if units_file:
            self.units_file = units_file
            self._load_units_convertion()
        else:
            self.units = get_units()
        
        self.types = get_types()
        self._load_data()
    
    def __dict__(self):
        return {
                'case': self.filename,
                'convert': self.convert_to_si
            }
    
    def _load_data(self):
        data = pd.read_csv(self.filename, sep='\t', dtype = str, na_values = nan_values)
        data = data.fillna(-1)

        # convert to correct types
        for k,t in self.types.items():
            if k in data:
                data[k] = data[k].astype(t)

        # convert to SI
        if self.convert_to_si:
            unit_convertion = self.units
            data['MEASURED_VALUE'] *= normalize(data['MEASURED_UNIT'], unit_convertion)
            data['LOQ_VALUE'] *= normalize(data['LOQ_UNIT'], unit_convertion)
            data['LOD_VALUE'] *= normalize(data['LOD_UNIT'], unit_convertion)
        
        self.data = data
    
    def _load_units_convertion(self):
        try:
            df = pd.read_csv(self.units_file, sep='\t')
            units = defaultdict(lambda:1)
            for k,f,mf in zip(df['ORIGINAL_UNIT'],df['FACTOR'],df['MATRIX_FACTOR']):
                factor = 1
                try:
                    f = float(f)
                    if f != 0 and f:
                        factor *= f
                except:
                    pass
                
                try:
                    mf = float(mf)
                    if f != 0 and f:
                        factor *= f
                except:
                    pass
                    
                units[k] = factor
                
        except FileNotFoundError:
            print(self.units_file, 'not found')
            units = defaultdict(lambda:1)
        
        self.units = units
        
    
    def unit_convertion(self, value, unit):
        """
        Converts a value into standard units.
        args:
            value :: float
            unit :: str 
        return
            float, value in new unit.
        """
        if unit in self.units:
            factor = self.units[unit]
        else:
            tmp = [(k,len(set(str(unit)) & set(str(k)))/len(set(str(unit)) | set(str(k)))) for k in self.units]
            tmp = sorted(tmp, key=lambda x:x[1], reverse = True)
            k,s = tmp.pop(0)
            if s > 0.6:
                factor = self.units[k]
            else:
                factor = -1
        
        return value*factor
    
    def chemicals(self):
        return set(self.data['CHEMICAL_ID']) - set([-1])
    
    def species(self):
        return set(self.data['SAMPLE_ORGANISM_TAX_ID']) - set([-1])
    
    def get_data(self):
        # splits data into domains, i.e. water sample, biota, sediment etc.
        
        out = defaultdict(list)
        for taxid,tissue,chemid,val,lod,m in zip(self.data['SAMPLE_ORGANISM_TAX_ID'],self.data['SAMPLE_TISSUE'] , self.data['CHEMICAL_ID'], self.data['MEASURED_VALUE'], self.data['LOD_VALUE'], self.data['SAMPLE_MATRIX']):
            val = max(val,lod)
            # skip nan values
            if val > 0:
                if taxid < 0:
                    out[m].append((chemid,val))
                else:
                    out[m].append((taxid,tissue,chemid,val))
        
        return out

