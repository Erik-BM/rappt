#ecotox

from collections import defaultdict
import pandas as pd
import os
from rdflib import Graph, Literal, Namespace, URIRef, BNode
from rdflib.namespace import FOAF, RDF, OWL, RDFS
UNIT = Namespace('http://qudt.org/vocab/unit#')
from rdflib.plugins.sparql import prepareQuery
from .utils import get_endpoints, test_endpoint, prefixes, query_endpoint, query_graph, reverse_mapping, strip, deprecated
import re

from .keys import get_time_units
time_units = get_time_units()

from fuzzywuzzy import fuzz, process
import warnings

import itertools
import time
from tqdm import tqdm

from SPARQLWrapper import SPARQLWrapper, JSON, TURTLE

nan_values = ['nan', float('nan'),'--','-X','NA','NC',-1,'','sp.', -1,'sp,','var.','variant','NR']

numeric_const_pattern = '[-+]? (?: (?: \d* \. \d+ ) | (?: \d+ \.? ) )(?: [Ee] [+-]? \d+ ) ?'
rx = re.compile(numeric_const_pattern, re.VERBOSE)

EXT = '.utf8.converted'
FORMAT = 'nt'


def score_function(args):
    k1,k2,names1,names2 = args
    score = 0
    for _n1 in names1:
        a = _n1.lower()
        for _n2 in names2:
            b = _n2.lower()
            s1 = jaccard(a,b)
            s2 = fuzz.ratio(a,b)/100
            score = max(score, s1*s2)
    return k1,k2,score
        
def mapping_args(names,out):
    with tqdm(total=len(names), desc='Mapping to NCBI') as pbar:
        for k1 in names:
            pbar.update(1)
            for k2 in out:
                yield k1,k2,names[k1],out[k2]
        
def get_wikidata_name(ids):
    
    s = ','.join(['\"'+ str(i) +'\"' for i in ids])
    
    endpoint = 'https://query.wikidata.org/sparql'
    q = """
    SELECT ?taxid ?name WHERE {
    SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". }
    ?item wdt:P685 ?taxid . 
    ?item wdt:P225 ?name .
    FILTER (?taxid in ( %s ))
    }
    """ % s
    
    try:
        res = query_endpoint(endpoint, q, var = ['taxid','name'])
    except:
        return []
    
    out = []
    for i in res:
        out.append(i)
    return out

def get_wikidata_inchikey_name():
    
    out = defaultdict(list)
    endpoint = 'https://query.wikidata.org/sparql'
    
    q = """
    SELECT DISTINCT ?inchikey ?label WHERE {
    ?compound wdt:P31 wd:Q11173 .
    ?compound wdt:P235 ?inchikey .
    ?compound skos:altLabel|rdfs:label ?label .
    FILTER(LANG(?label) = "en") 
    }"""

    try:
        res = query_endpoint(endpoint, q, var = ['inchikey','label'])
    except:
        res = []

    for i,l in res:
        out[i].append(l)
    
    return out

def jaccard(A, B):
    return len(set(A) & set(B))/len(set(A) | set(B))

class Ecotox:
    """
    Provides easy access to ecotox data.
    """
    def __init__(self, directory = '', endpoint = '', rdf_file = '', namespace = 'http://example.org/ecotox', endpoints_to_consider = None, effects_to_consider = None, verbose = False, testing = False):
        """
        args:
            directory :: str 
                data directory, must contain results and tests files as well as validation subdirectory.
            endpoint :: str
                sparql endpoint. If endpoint is provided, files will not be loaded.
            namespace :: str   
                base namespace for the dataset.
        """
        if not test_endpoint(endpoint):
            self.endpoint = ''
        self.testing = testing
        self.directory = directory
        self.results_file = directory + 'results' + EXT
        self.tests_file = directory + 'tests' + EXT
        self.species_file = directory + 'validation/species' + EXT
        self.species_synonyms_file = directory + 'validation/species_synonyms' + EXT
        self.endpoints_file = directory + 'validation/endpoint_codes' + EXT
        self.chemical_name_file = directory + 'validation/chemicals' + EXT
        self.units_file = directory+'RA_UNIT_CONVERSIONS.tsv'
        
        self.namespace = Namespace(namespace+'/')
        self.onto_namespace = Namespace(namespace+'#')
        self.endpoint = endpoint
        self.initNs = {'rdf':RDF, 'foaf':FOAF, 'ns':self.namespace, 'nso':self.onto_namespace, 'owl':OWL, 'rdfs':RDFS, 'unit':UNIT}
        
        self.endpoints_to_consider = endpoints_to_consider
        self.effects_to_consider = effects_to_consider
        self.verbose = verbose
        
        if not endpoint:
            self.graph = Graph()
            if rdf_file:
                print('Parsing',rdf_file)
                self.graph.parse(rdf_file, format=FORMAT)
            if directory:
                self._load_data_v2()
                self._load_chemicals()
                self._load_species()
                self._load_hierarchy()
                self._sanity_checks()
            else:
                warnings.warn('No data is loaded.')
                
            self._load_units_convertion()
        
        if not self.endpoint:
            for k,i in self.initNs.items():
                self.graph.bind(k,i)
    
    def __dict__(self):
        return {
                'namespace':self.namespace,
                'directory': self.directory,
                'endpoint': self.endpoint,
                'namespaces': self.initNs
            }
    
    def _sanity_checks(self):
        pass
    
    def _load_units_convertion(self):
        try:
            df = pd.read_csv(self.units_file, sep='\t')
            units = defaultdict(lambda:1)
            for k,f,mf in zip(df['ORIGINAL_UNIT'],df['FACTOR'],df['MATRIX_FACTOR']):
                factor = 1
                try:
                    f = float(f)
                    if f != 0 and f:
                        factor *= f
                except:
                    pass
                
                try:
                    mf = float(mf)
                    if f != 0 and f:
                        factor *= f
                except:
                    pass
                    
                units[k] = factor
                
        except FileNotFoundError:
            print(self.units_file, 'not found')
            units = None
        
        self.units = units
        
    def _load_data_v2(self):
        g = Graph()
        endpoints = get_endpoints(self.endpoints_file)
        tests = pd.read_csv(self.tests_file, sep='|', low_memory=False, dtype = str).fillna(-1)
        results = pd.read_csv(self.results_file, sep='|', low_memory=False, dtype = str).fillna(-1)
        
        if self.verbose:
            pbar = tqdm(total = len(tests['test_id']), desc='Loading test data')
        else:
            pbar = None
            
        i = 0
        
        for test_id, cas_number, species_number, stdm, stdu, habitat, lifestage, age, ageunit, weight, weightunit in zip(tests['test_id'],
        tests['test_cas'],
        tests['species_number'],
        tests['study_duration_mean'],
        tests['study_duration_unit'],
        tests['organism_habitat'],
        tests['organism_lifestage'],
        tests['organism_age_mean'],
        tests['organism_age_unit'],
        tests['organism_init_wt_mean'],
        tests['organism_init_wt_unit']):
            
            i += 1
            if self.testing and i > 100: 
                break
            
            if pbar: pbar.update(1)
            
            if any([a in nan_values for a in [test_id,cas_number,species_number]]):
                continue
            
            t = self.namespace['test/'+str(test_id)]
            s = self.namespace['taxon/'+str(species_number)]
            c = self.namespace['chemical/'+str(cas_number)]
            
            g.add((t, RDF.type, self.onto_namespace['Test']))
            g.add((t, self.onto_namespace['species'], s))
            g.add((t, self.onto_namespace['chemical'], c))
            
            for v,u,p in zip([stdm,age,weight],[stdu,ageunit,weightunit],['studyDuration','organismAge','organismWeight']):
                if not v in nan_values:
                    b = BNode()
                    g.add( (b, RDF.value, Literal(v)) )
                    g.add( (b, UNIT.units, Literal(u)) )
                    g.add( (t, self.onto_namespace[p], b) )
                
            if not habitat in nan_values:
                g.add((t, self.onto_namespace['organismHabitat'],self.namespace['habitat/'+habitat]))
                
            if not lifestage in nan_values:
                g.add((t, self.onto_namespace['organismLifestage'],self.namespace['lifestage/'+lifestage]))
        
        if self.verbose:
            pbar = tqdm(total = len(results['test_id']), desc='Loading results data')
        else:
            pbar = None
            
        i = 0
            
        for test_id, endpoint, conc, conc_unit, effect in zip(results['test_id'], results['endpoint'], results['conc1_mean'], results['conc1_unit'], results['effect']):
            i += 1
            if self.testing and i > 100: 
                break
            
            if pbar: pbar.update(1)
            
            if endpoint in nan_values:
                    continue
                
            if self.endpoints_to_consider:
                if not any([a.lower() in str(endpoint).lower() for a in self.endpoints_to_consider]):
                    continue
                
            if self.effects_to_consider:
                if not any([a.lower() in str(effect).lower() for a in self.effects_to_consider]):
                    continue
            
            if conc in nan_values:
                continue
            else:
                conc = rx.findall(conc)
                if len(conc) > 0:
                    conc = conc.pop(0)
                else:
                    continue
            
            t = self.namespace['test/'+str(test_id)]
            
            r = BNode()
            ep = self.namespace['endpoint/'+str(endpoint)]
            ef = self.namespace['effect/'+str(effect)]
            
            g.add((r,self.onto_namespace['endpoint'],ep))
            g.add((r,self.onto_namespace['effect'],ef))
            if not conc_unit in nan_values:
                b = BNode()
                g.add( (b, RDF.value, Literal(conc)) )
                g.add( (b, UNIT.units, Literal(conc_unit)) )
                g.add( (r, self.onto_namespace['concentration'], b) )
            
            g.add((t,self.onto_namespace['hasResult'],r))
        
        self.graph += g
        
    
    def _load_species(self):
        # creates a set of names for each species id. 
        
        g = Graph()
        
        species = defaultdict(set)
        df1 = pd.read_csv(self.species_file, sep='|', low_memory=False, dtype = str).fillna(-1)
        
        if self.verbose:
            pbar = tqdm(total = len(df1['species_number']), desc='Loading species')
        else:
            pbar = None
        
        i = 0
            
        for s, cn, ln, group in zip(df1['species_number'], df1['common_name'], df1['latin_name'],df1['ecotox_group']):
            i += 1
            if self.testing and i > 100: 
                break
            if pbar: pbar.update(1)
            if s in nan_values:
                continue
            s = self.namespace['taxon/'+s]
            if not group in nan_values:
                names = group.split(',')
                group = group.replace('/','')
                group = group.replace('.','')
                group = group.replace(' ','')
                tmp = group.split(',')
                group_uri = [self.namespace['group/'+gr] for gr in tmp]
                
                for gri,n in zip(group_uri,names):
                    g.add((s, RDFS.subClassOf, gri))
                    g.add((gri, RDFS.label, Literal(n)))
                    g.add((gri, RDF.type, self.onto_namespace['SpeciesGroup']))
                
            g.add((s, RDF.type, self.onto_namespace['Taxon']))
                
            if not cn in nan_values:
                g.add((s, self.onto_namespace['commonName'], Literal(cn)))
            if not ln in nan_values:
                g.add((s, self.onto_namespace['latinName'], Literal(ln)))
        
        g.add((self.onto_namespace['commonName'], RDFS.subPropertyOf, RDFS.label))
        g.add((self.onto_namespace['latinName'], RDFS.subPropertyOf, RDFS.label))
        
        self.graph += g
    
    def _load_chemicals(self):
        # all chemical ids in dataset.
        tests = pd.read_csv(self.chemical_name_file, sep='|', low_memory=False, dtype = str).fillna(-1)
       
        g = Graph()
        if self.verbose:
            pbar = tqdm(total = len(tests['cas_number']), desc='Loading chemicals')
        else:
            pbar = None
        i = 0
            
        for c, n, group in zip(tests['cas_number'], tests['chemical_name'],tests['ecotox_group']):
            i += 1
            if self.testing and i > 100: 
                break
            if pbar: pbar.update(1)
            if not any([c in nan_values, n in nan_values]):
                c = self.namespace['chemical/'+str(c)]
                g.add((c, RDF.type, self.onto_namespace['Chemical']))
                if n:
                    for a in n.split(', '):
                        g.add((c, RDFS.label, Literal(a)))
                    
                if not group in nan_values:
                    names = group.split(',')
                    group = group.replace('/','')
                    group = group.replace('.','')
                    group = group.replace(' ','')
                    tmp = group.split(',')
                    group_uri = [self.namespace['group/'+gr] for gr in tmp]
                
                    for gri,n in zip(group_uri,names):
                        g.add((c, RDFS.subClassOf, gri))
                        g.add((gri, RDFS.label, Literal(n)))
                        g.add((gri, RDF.type, self.onto_namespace['ChemicalGroup']))
                
        self.graph += g
        
    def _load_hierarchy(self):
        g = Graph()
        tmp = set()
        df = pd.read_csv(self.species_file, sep= '|', low_memory = False, dtype = str).fillna(-1)
        ranks = ['variety','subspecies','species','genus','family','tax_order','class','superclass','subphylum_div','phylum_division','kingdom']
        #ranks = ranks[::-1]
        if self.verbose:
            pbar = tqdm(total = len(df['species_number']), desc='Loading hierarchy')
        else:
            pbar = None
            
        i = 0
            
        for sn, lineage in zip(df['species_number'], zip(df['variety'],df['subspecies'],df['species'],df['genus'],df['family'],df['tax_order'],df['class'],df['superclass'],df['subphylum_div'],df['phylum_division'],df['kingdom'])):
            i += 1
            if self.testing and i > 100: 
                break
            if pbar: pbar.update(1)
            curr = self.namespace['taxon/'+sn]
            g.add((curr, RDF.type, self.onto_namespace['Taxon']))
            for i, a in enumerate(lineage):
                tmp.add(curr)
                if not a in nan_values:
                    n = a
                    #l = set(self.graph.subjects(predicate=RDFS.label,object=Literal(n))) & tmp
                    #try:
                        #a = l.pop()
                        #self.add_same_as_species(l)
                    #except KeyError:
                    a = a.replace(' ','_')
                    a = self.namespace['taxon/'+a]
                    g.add((a, RDFS.label, Literal(n)))
                    g.add((a, RDF.type, self.onto_namespace['Taxon']))
                    r = self.namespace['rank/'+ranks[i]]
                    g.add((a, self.onto_namespace['rank'], r))
                    g.add((r, RDFS.label, Literal(ranks[i])))
                    g.add((r, RDF.type, self.onto_namespace['Rank']))
                    g.add((curr, RDFS.subClassOf, a))
                    curr = a
                
        self.graph += g
        
    ### PRIVATE FUNCTIONS
        
    def _query(self, q, var):
        if self.endpoint:
            results = query_endpoint(self.endpoint, q, var = var)
        else:
            results = query_graph(self.graph, q)
        
        if len(var) < 2:
            results = [r[0] for r in results]
        
        return results
        
        
    def _get_parent(self, taxon):
        q = prefixes(self.initNs)
        q += """
            SELECT ?p WHERE {
                <%s> rdf:type | rdfs:subClassOf ?p .
            }
        """ % taxon
        out = self._query(q, ['p'])
        return out
    
    def _lineage(self, taxon, depth):
        out = []
        p = self._get_parent(taxon)
        
        if depth > 10 or not p:
            return out
        
        if taxon in p:
            p.remove(taxon)
        if len(p) == 1:
            out.append({'child':taxon, 'parent': p[0], 'score':1})
        else:
            for s in p:
                out.append({'child':taxon, 'parent': s, 'score':1})
                out.extend(self._lineage(s, depth = depth + 1))
        
        return out
    
    def _hierarchy(self, species):
        out = []
            
        for curr in species:
            try:
                out.extend(self._lineage(curr, depth = 0))
            except RecursionError:
                pass
        
        out = [a for a in out if a]
        return out
    
    
    def _chemicals(self):
        # return all chemicals
        sparql = SPARQLWrapper(self.endpoint)
        q = prefixes(self.initNs)
        q += """
            SELECT DISTINCT ?c WHERE {
                ?c rdf:type nso:Chemical
            }
        """
        results = self._query(q, ['c'])
            
        out = set()
        for row in results:
            out.add(row)
        return out
    
    def _species(self):
        # return all species
        sparql = SPARQLWrapper(self.endpoint)
        q = prefixes(self.initNs)
        q += """
            SELECT DISTINCT ?c WHERE {
                ?c rdf:type nso:Taxon
            }
        """
        results = self._query(q, ['c'])
        out = set()
        for row in results:
            out.add(row)
        return out
    
    def _species_names(self, subset = None):
        # return names of species.
        sparql = SPARQLWrapper(self.endpoint)
        initNs = self.initNs
        q = prefixes(initNs)
        if subset:
            q += """
                SELECT ?c ?ln {
                    values ?c { %s }
                    ?c rdf:type nso:Taxon ;
                        nso:latinName ?ln .
                }
            """ % '{0}'.format(' '.join(['<'+str(s)+'>' for s in subset]))
        else:
            q += """
            SELECT ?c ?ln WHERE {
                ?c rdf:type nso:Taxon ;
                    nso:latinName ?ln . 
            }
        """
        
        results = self._query(q, ['c','ln'])
            
        out = defaultdict(set)
        for s,n in results:
            out[s].add(str(n))
        return out
    
    def _chemical_names(self, subset = None):
        # return names of species.
        sparql = SPARQLWrapper(self.endpoint)
        initNs = {'rdf':RDF, 'foaf':FOAF, 'ns':self.namespace}
        q = prefixes(initNs)
        if subset:
            q += """
                SELECT ?c ?n {
                    values ?c { %s }
                    ?c rdf:type nso:Chemical ;
                        rdfs:label ?n .
                }
            """ % '{0}'.format(' '.join(['<'+str(s)+'>' for s in subset]))
        else:
            q += """
            SELECT ?c ?n WHERE {
                ?c rdf:type nso:Chemical ;
                    rdfs:label ?n .
            }
        """
        
        results = self._query(q, ['c','n'])
            
        out = defaultdict(set)
        for s,n in results:
            out[s].add(str(n))
        return out
    
    def _endpoint(self, c, s):
        q = prefixes(self.initNs)
        q += """
            SELECT ?c ?s ?cc ?cu ?ep ?ef ?sd ?sdu WHERE {
                ?test rdf:type nso:Test ;
                  nso:chemical <%s> ;
                   nso:species <%s> ;
                   nso:hasResult [ 
                   nso:endpoint ?ep ;
                   nso:effect ?ef ;
                   nso:concentration [rdf:value ?cc ; 
                                        unit:units ?cu] ] .
               
                OPTIONAL {
                    ?test nso:studyDuration [rdf:value ?sd ;
                                            unit:units ?sdu] .
                }
            }""" % (str(c), str(s))
        
        out = []
        for tmp in self._query(q, ['cc','cu','ep','ef','sd','sdu']):
            out.append([c,s].extend(tmp))
        
        return out
    
    def _endpoint_no_chemical(self, s):
        q = prefixes(self.initNs)
        q += """
            SELECT ?c ?s ?cc ?cu ?ep ?ef ?sd ?sdu {
            VALUES ?s { <%s> }
                ?test rdf:type nso:Test ;
                   nso:chemical ?c ;
                   nso:species ?s ;
                   nso:hasResult [ 
                   nso:endpoint ?ep ;
                   nso:effect ?ef ;
                   nso:concentration [rdf:value ?cc ; 
                                        unit:units ?cu] ] .
               
                OPTIONAL {
                    ?test nso:studyDuration [rdf:value ?sd ;
                                            unit:units ?sdu] .
                }
            }""" % str(s)
            
        return self._query(q, ['c','s','cc','cu','ep','ef','sd','sdu'])
            
    def _endpoint_no_taxon(self, c):
        q = prefixes(self.initNs)
        q += """
            SELECT ?c ?s ?cc ?cu ?ep ?ef ?sd ?sdu {
            VALUES ?c { <%s> }
                ?test rdf:type nso:Test ;
                   nso:chemical ?c ;
                   nso:species ?s ;
                   nso:hasResult [ 
                   nso:endpoint ?ep ;
                   nso:effect ?ef ;
                   nso:concentration [rdf:value ?cc ; 
                                        unit:units ?cu] ] .
               
                OPTIONAL {
                    ?test nso:studyDuration [rdf:value ?sd ;
                                            unit:units ?sdu] .
                }
            }""" % str(c)
        
        return self._query(q, ['c','s','cc','cu','ep','ef','sd','sdu'])
        
    
    def _all_endpoints(self):
        q = prefixes(self.initNs)
        q += """
            SELECT ?c ?s ?cc ?cu ?ep ?ef ?sd ?sdu WHERE {
                ?test rdf:type nso:Test ;
                   nso:chemical ?c ;
                   nso:species ?s ;
                   nso:hasResult [ 
                   nso:endpoint ?ep ;
                   nso:effect ?ef ;
                   nso:concentration [rdf:value ?cc ; 
                                        unit:units ?cu] ] .
               
                OPTIONAL {
                    ?test nso:studyDuration [rdf:value ?sd ;
                                            unit:units ?sdu] .
                }
            }"""
            
        return self._query(q, ['c','s','cc','cu','ep','ef','sd','sdu'])
    
        
    def _endpoints(self, chems, species):
        
        if not chems and not species:
            d = self._all_endpoints()
        elif not chems and species:
            d = []
            for s in species:
                d.extend(self._endpoint_no_chemical(s))
        elif chems and not species:
            d = []
            for s in chems:
                d.extend(self._endpoint_no_taxon(s))
        else:
            d = []
            for c in chems:
                for s in species:
                    d.extend(self._endpoint(c,s))
        
        out = []
        for c,s,cc,cu,ep,ef,sd,sdu in d:
            d = {'cas':str(c), 'taxon':str(s), 'endpoint':str(ep), 'effect':str(ef), 'concentration':float(cc), 'concentration_unit':str(cu)}
            if sd:
                d['study_duration'] = float(sd)
                d['study_duration_unit'] = str(sdu)                        
            out.append(d)
        
        return out
    
    def _save(self, directory = './', f = FORMAT):
        if self.endpoint:
            raise NotImplementedError ('save from endpoint is not implemented.')
        else: 
            self.graph.serialize(directory + 'ecotox.' + f , format = f)
     
    def _get_same_as_species(self, i):
        q = prefixes(self.initNs)
        q += """
            SELECT ?s WHERE {
                <%s> owl:sameAs ?s ;
                     rdf:type nso:Taxon .
            }
        """
        return self._query(q, ['s'])
    
    def _get_same_as_chemical(self, i):
        q = prefixes(self.initNs)
        q += """
            SELECT ?s WHERE {
                <%s> owl:sameAs ?s ;
                     rdf:type nso:Chemical .
            }
        """
        return self._query(q, ['s'])
    
    def _map_ncbi_taxid(self, taxids):
        tmp = get_wikidata_name(taxids)
        out = {}
        for i,s in tmp:
            q = prefixes(self.initNs)
            q += """
                SELECT DISTINCT ?id WHERE {
                ?id rdfs:label ?name ;
                    rdf:type nso:Taxon .
                FILTER regex(?name, "%s", "i")
                }
            """ % str(s)
            
            out[i] = self._query(q, ['id'])
        return out
    
    def _map_to_inchikey(self, names):
        # from cas to inchikey thru wikidata names. 
        
        if hasattr(self,'wikidata_inchikey_names'):
            d = self.wikidata_inchikey_names
        else:
            d = get_wikidata_inchikey_name()
            self.wikidata_inchikey_names = d
            
        out = []
        
        for k2 in d:
            for k1 in names:
                c = set(d[k2]) & set(names[k1])
                if len(c) > 0:
                    out.append((k1,k2,'|'.join([str(a) for a in c])))
                    
        return out
    
    def _classes(self):
        q = prefixes(self.initNs)
        q += """
            SELECT ?c WHERE {
                ?c a owl:Class .
            }
        """
        results = self._query(q, ['c'])
        return set(results)
    
    def _class_label(self, c):
        q = prefixes(self.initNs)
        q += """
            SELECT ?l WHERE {
                <%s> rdfs:label ?l .
            } 
        """ % c
        return self._query(q, ['l'])
    
    ### PUBLIC FUNCTIONS
    ### convert - add namespace/remove namespace
    
    def ecotox_to_ncbi(self, ids, convert = False):
        """
        Convert ecotox species ids to ncbi ids via wikidata species names.
        """
        names = self.species_names(ids, convert = convert)
    
        out = defaultdict(list)
        endpoint = 'https://query.wikidata.org/sparql'
        
        if hasattr(self, 'ecotox_to_ncbi_mapping'):
            out = self.ecotox_to_ncbi_mapping
        
        else:
            q = """
                SELECT ?ncbi ?name WHERE {
                SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". }
                ?item wdt:P685 ?ncbi . 
                ?item wdt:P225 ?name .
                }
                """
            try:
                res = query_endpoint(endpoint, q, var = ['ncbi','name'])
            except:
                res = []
            
            for i in res:
                nc, name = i
                out[nc].append(name)
                
            self.ecotox_to_ncbi_mapping = out
            
        
        if hasattr(self, 'mappings'):
            mappings = self.mappings
        
        else:
            mappings = defaultdict(list)
            scores = map(score_function, mapping_args(names,out))
            
            for k1,k2,score in scores:
                if score > 0.8:
                    mappings[k1].append((k2,score))
            
            self.mappings = mappings
        
        out = {}
        
        for k in names:
            if k in mappings:
                tmp = sorted(mappings[k], key=lambda x:x[1], reverse=True)
                out[k] = tmp.pop(0)[0]
            else:
                out[k] = 'no mapping'
        
        return out
        
    
    def unit_convertion(self, value, unit):
        """
        Converts a value into standard units (mg/L).
        args:
            value :: float
            unit :: str 
        return
            float, value in new unit.
        """
        if not self.units:
            warnings.warn('No units mapping exists. Returning value.')
            return value
        
        if unit in self.units:
            factor = self.units[unit]
        else:
            tmp = [(k,len(set(str(unit)) & set(str(k)))/len(set(str(unit)) | set(str(k)))) for k in self.units]
            tmp = sorted(tmp, key=lambda x:x[1], reverse = True)
            k,s = tmp.pop(0)
            if s > 0.7:
                factor = self.units[k]
            else:
                factor = 1
        
        return value*factor
    
    def add_same_as_chemicals(self, ids, convert = False):
        """
        Adds sameAs relations between all chemicals in ids.
        args:
            ids :: list
            convert :: boolean
                whether to add namespace to ids
        """
        if convert: ids = [self.namespace['chemical/'+str(s)] for s in ids]
        for a,b in itertools.product(ids, ids):
            self.graph.add((URIRef(a), OWL.sameAs, URIRef(b)))
            
    def add_same_as_species(self, ids, convert = False):
        """
        Adds sameAs relations between all species in ids.
        args:
            ids :: list
            convert :: boolean
                whether to add namespace to ids
        """
        if convert: ids = [self.namespace['taxon/'+str(s)] for s in ids]
        for a,b in itertools.product(ids, ids):
            self.graph.add((URIRef(a), OWL.sameAs, URIRef(b)))
            
    def classes(self):
        """
        All classes of species.
        returns:
            str 
                class URI
        """
        return self._classes()
    
    def class_label(self, c):
        """
        Returns name of class.
        args:
            c :: str 
                class URI
        returns:
            str
        """
        return self._class_label(c)
    
    def hierarchy(self, ids, convert = False):
        """
        Construct hierarchy of species.
        args:
            ids :: list 
                list of species
            convert :: boolean
                whether to add namespace to ids
        returns:
            out :: list
                list of child, parent, score triples stored in dict.
        """
        if convert: ids = [self.namespace['taxon#'+str(s)] for s in ids]
        out = self._hierarchy(ids)
        if convert: out = [{k:strip(d[k], ['#', '/']) for k in d} for d in out]
        return out
    
    def construct_tree(self, subset = None, convert = False):
        """
        Synonym for hierarchy.
        """
        return self.hierarchy(subset, convert)
    
    def map_ncbi_taxid(self, taxids, convert = False):
        """
        Convert ecotox tax ids to ncbi tax ids
        args:
            taxids :: list
                list of ecotox tax ids
            convert :: boolean
                whether to remove namespace from ids
        returns:
            out :: dict
                key - ecotox taxid
                item - ncbi taxid(s)
        """
        if convert: taxids = [t.split('#')[-1] for t in taxids]
        out = self._map_ncbi_taxid(taxids)
        if convert: out = {k:[strip(o,'#') for o in out[k]] for k in out}
        return out
        
    def map_to_inchikey(self, casids, convert = False):
        """
        Convert ecotox tax ids to ncbi tax ids
        args:
            taxids :: list
                list of ecotox tax ids
            convert :: boolean
                whether to remove namespace from ids
        returns:
            out :: dict
                key - ecotox taxid
                item - ncbi taxid(s)
        """
        names = self.chemical_names(casids, convert = convert)
        out = self._map_to_inchikey(names)
        
        return out
        
    def get_same_as_species(self, ids, convert = False):
        """
        Get identifiers of all equal species.
        args:
            ids :: list
                species
            convert :: boolean
                whether to add namespace to input.
        returns:
            out :: dict
                key - species
                item - list of species that are the same as key.
        """
        if convert: ids = [self.namespace['taxon/'+str(s)] for s in ids]
        try:
            out = {i:set(self._get_same_as_species(i)) for i in ids}
        except TypeError:
            return set()
        if convert: out = {strip(k,['/','#']):[strip(a,['/','#']) for a in out[k]] for k in out}
        return out
     
    def get_same_as_chemical(self, ids, convert = False):
        """
        Get identifiers of all equal chemicals.
        args:
            ids :: list
                chemicals
            convert :: boolean
                whether to add namespace to input.
        returns:
            out :: dict
                key - chemical
                item - list of chemicals that are the same as key.
        """
        if convert: ids = [self.namespace['chemical/'+str(s)] for s in ids]
        try:
            out = {i:set(self._get_same_as_chemical(i)) for i in ids}
        except TypeError:
            return set()
        if convert: out = {strip(k,['/','#']):[strip(a,['/','#']) for a in out[k]] for k in out}
        return out
        
    def endpoints(self, chems = None, species = None, convert = False):
        """
        Return all experiment endpoint in ecotox involving chems and species.
        args:
            chems :: list
                chemical list
            species :: list
                species list
            convert :: boolean
                whether to add namespace to inputs.
        returns:
            out :: list
                dict containing
                cas, taxon, endpoint, concentration, concentration_unit
        """
        if convert:
            if chems:
                chems = [self.namespace['chemical/'+str(c)] for c in chems]
            if species:
                species = [self.namespace['taxon/'+str(s)] for s in species]
        out = self._endpoints(chems, species)
        if convert:
            out = [{k:strip(s[k],['/','#']) for k in s} for s in out]
        return out
        
    def species(self, convert = False):
        """
        Return all species in dataset.
        args:
            convert :: boolean
                whether to strip namespace on output.
        returns:
            out :: set
                all species
        """
        out = self._species()
        if convert: out = set([strip(s,['/','#']) for s in out])
        return out
        
    def chemicals(self, convert = False):
        """
        Return all chemicals in dataset.
        args:
            convert :: boolean
                whether to strip namespace on output.
        returns:
            out :: set
                all chemicals
        """
        out = self._chemicals()
        if convert: out = set([strip(s,['/','#']) for s in out])
        return out
    
    def chemicals_to_species(self, chems, convert = False):
        """
        Return all species a chemical has an effect on.
        args:
            chems :: list
                chemicals
            convert :: boolean 
                whether to add namespace to input.
        returns:
            out : dict
                key - chemical
                item - effected species
        """
        if convert: chems = [self.namespace['chemical/'+str(c)] for c in chems]
        out = self._chemicals_to_species(chems)
        out = {k:out[k] for k in out if out[k]}
        if convert: out = {strip(str(k),['/','#']):[strip(o,['/','#']) for o in out[k]] for k in out}
        return out
    
    def species_to_chemicals(self, species, convert = False):
        """
        Return all chemicals that has an effect of a species.
        args:
            species :: list
                chemicals
            convert :: boolean 
                whether to add namespace to input.
        returns:
            out : dict
                key - species
                item - effected by chemical
        """
        if convert: species = [self.namespace['taxon/'+str(s)] for s in species]
        out = self._species_to_chemicals(species)
        if convert: out = {strip(k,['/','#']):[strip(o,['/','#']) for o in out[k]] for k in out}
        return out
    
    def species_names(self, subset = None, convert = False):
        """
        Return names of species.
        args:
            subset :: list
                species
            convert :: boolean
                whether to add namespace to input
        returns:
            out :: dict
                key - species
                item - list of names
        """
        if convert and subset: subset = set([self.namespace['taxon/'+str(s)] for s in subset])
        out = self._species_names(subset)
        if convert: out = {strip(k,['/','#']):[strip(o,['/','#']) for o in out[k]] for k in out}
        return out 
        
    def chemical_names(self, subset = None, convert = False):
        """
        Return names of chemicals.
        args:
            subset :: list
                chemicals
            convert :: boolean
                whether to add namespace to input
        returns:
            out :: dict
                key - chemical
                item - list of names
        """
        if convert and subset: subset = set([self.namespace['chemical/'+str(s)] for s in subset])
        if subset:
            d = self.get_same_as_chemical(subset)
            s = set.union(*[set(d[k]) for k in d])
            subset |= s
        out = self._chemical_names(subset)
        if convert: out = {strip(k,['/','#']):[strip(o,['/','#']) for o in out[k]] for k in out}
        return out 
        
    def save(self, directory = None, f = FORMAT):
        """
        Save loaded data to files.
        args:
            directory :: str
        """
        return self._save(directory, f)
    
    def query(self, q, var):
        """
        Run any query over the db.
        args:
            q :: str 
                query
            var :: list
                list of variables to return from query.
        returns:
            list 
                list of tuples of len(var)
        
        eg.
        >>> q = 'select ?s ?c where {?s a ?c}'
        >>> var = ['s','c']
        >>> query(q, var)
        [(123, animal), (321, fish)]
        """
        q = prefixes(self.initNs) + q
        return self._query(q, var)
        

        

