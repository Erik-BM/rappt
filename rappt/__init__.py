# -*- coding: utf-8 -*-
__version__ = '0.1.0'

from .taxonomy import Taxonomy
from .ecotox import Ecotox
from .chemicals import Chemistry
from .alignment import Alignment
from .casestudy import CaseStudy
from .utils import *
from .strings import *
from .keys import *
