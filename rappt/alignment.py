### alignment API

from rdflib import Graph, BNode, URIRef
from rdflib.namespace import OWL, RDF
from collections import defaultdict
from .utils import strip

class Alignment:
    def __init__(self, logmap_file = None, threshold=0.9):
        self._load_alignment(logmap_file, threshold)
    
    def _load_alignment(self, filename, threshold):
        """
        Convert LogMap file to mapping dict. 
        filename :: str 
            Path to logmap2 output rdf file. rdflib does not support .owl files.
        threshold :: float
            Above which threshold to create mapping. 
        """
        
        g = Graph()
        g.parse(filename)
        
        mapping1 = defaultdict(list)
        mapping2 = defaultdict(list)
        scores = defaultdict(float)
        
        o = URIRef('http://knowledgeweb.semanticweb.org/heterogeneity/alignmentCell')
        
        for s in g.subjects(predicate=RDF.type, object = o):
            e1 = list(g.objects(subject=s,predicate=URIRef('http://knowledgeweb.semanticweb.org/heterogeneity/alignmententity1'))).pop(0)
            e2 = list(g.objects(subject=s,predicate=URIRef('http://knowledgeweb.semanticweb.org/heterogeneity/alignmententity2'))).pop(0)
            score = list(g.objects(subject=s,predicate=URIRef('http://knowledgeweb.semanticweb.org/heterogeneity/alignmentmeasure'))).pop(0)
            
            e1 = str(e1)
            e2 = str(e2)
            score = float(score)
            
            mapping1[e1].append((e2,score))
            
            scores[(e1,e2)] = score
        
        self.ecotox2ncbi = {}
        
        #If one2many mapping: keep the best match. 
        for k in mapping1:
            tmp = sorted(mapping1[k], key=lambda x:x[1], reverse=True)
            item,score = tmp.pop(0)
            if score >= threshold:
                self.ecotox2ncbi[k] = item
        
        self.ncbi2ecotox = {i:k for k,i in self.ecotox2ncbi.items()}
        
        self.scores = scores
        
    def ecotox2ncbi_mapping(self, convert = False):
        """
        Fetch mapping from ecotox to ncbi. 
        convert :: boolean
            Removes namespace.
        """
        if convert:
            tmp = {}
            for k in self.ecotox2ncbi:
                tmp[strip(k,'/')] = strip(self.ecotox2ncbi[k],'/')
        else:
            tmp = self.ecotox2ncbi
            
        return tmp
    
    def ncbi2ecotox_mapping(self, convert = False):
        """
        Fetch mapping from ncbi to ecotox. 
        convert :: boolean
            Removes namespace.
        """
        if convert:
            tmp = {}
            for k in self.ncbi2ecotox:
                tmp[strip(k,'/')] = strip(self.ncbi2ecotox[k],'/')
        else:
            tmp = self.ncbi2ecotox
            
        return tmp
        
        
