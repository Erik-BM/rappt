# chemical data api

import pandas as pd
from collections import defaultdict
import requests
from time import sleep

from tqdm import tqdm
import os

from pubchempy import Compound, get_compounds, get_substances
from .utils import test_endpoint, prefixes, query_endpoint, query_graph, reverse_mapping, strip
import pubchempy

import warnings

from rdflib import Graph, Namespace, RDF, URIRef
from SPARQLWrapper import SPARQLWrapper, JSON, TURTLE

FORMAT = 'nt'

global over_air_cid2inchikey
over_air_cid2inchikey = {}
global over_air_inchikey2cid
over_air_inchikey2cid = {}

def chunks(l, n):
    if isinstance(l, set):
        l = list(l)
    """Yield successive n-sized chunks from l."""
    for i in range(0, len(l), n):
        yield l[i:i + n]

def chemical_id2inchikey(chemical_file):
    chemical_data = pd.read_csv(chemical_file, sep='\t', dtype = str, usecols=['CHEMICAL_ID', 'INCHIKEY_STANDARD'])
    chemical_data = chemical_data.fillna(-1)
    out = {}
    for c,i in zip(chemical_data['CHEMICAL_ID'], chemical_data['INCHIKEY_STANDARD']):
        if all([c != -1, i != -1]):
            out[c] = i
    return out

def chemical_id2cas(chemical_file):
    chemical_data = pd.read_csv(chemical_file, sep='\t', dtype = str, usecols=['CHEMICAL_ID', 'CAS_NODASH'])
    chemical_data = chemical_data.fillna(-1)
    out = {}
    for c,i in zip(chemical_data['CHEMICAL_ID'], chemical_data['CAS_NODASH']):
        if all([c != -1, i != -1]):
            out[c] = i
    return out
    
def cas2inchikey(cas_file, chemical_file):
    
    chem2cas = chemical_id2cas(cas_file)
    chem2inchi = chemical_id2inchikey(chemical_file)
    
    out = {}
    for k,cas in chem2cas.items():
        out[cas] = chem2inchi[k]
    return out

        
def mapping_from_wikidata(out,p,ids):
    ids = set(ids) - set(["no mapping"])
    endpoint = 'https://query.wikidata.org/sparql'
    
    out = {}
    for chunk in chunks(ids, 100):
        s = ' '.join(['"'+str(i)+'"' for i in chunk])
        q = """
            SELECT ?inchikey ?cas
            { 
            VALUES ?cas { %s }
            ?compound wdt:P235 ?inchikey .
            ?compound %s ?cas .
            SERVICE wikibase:label { bd:serviceParam wikibase:language "en". }
            } 
        
        """ % (s,p)
        try:
            res = query_endpoint(endpoint, q, var = ['inchikey', 'cas'])
        except:
            res = {}
            
        for inchikey,cas in res:
            out[inchikey] = cas
    
    return out

def inchikey2cas(d,ids):
    t = apply_mapping(ids,d)
    if count_missing(t, None, None, verbose = False) > 0:
        ids = [i for i in ids if i not in t]
        tmp = mapping_from_wikidata(d,'wdt:P231',ids)
        return concat_dict(d,tmp)
    return d

def inchikey2mesh(d,ids):
    t = apply_mapping(ids,d)
    if count_missing(t, None, None, verbose = False) > 0:
        ids = [i for i in ids if i not in t]
        tmp = mapping_from_wikidata(d,'wdt:P486',ids)
        return concat_dict(d,tmp)
    return d
    
def inchikey2cid(d,ids):
    t = apply_mapping(ids,d)
    if count_missing(t, None, None, verbose = False) > 0:
        ids = [i for i in ids if i not in t]
        tmp = mapping_from_wikidata(d,'wdt:P486',ids)
        tmp = pubchem_inchikey2cid(tmp, ids)
        return concat_dict(d,tmp)
    return d
        
def remove_namespace(cid, c = 'CID'):
    return cid.split(c)[-1]
    
def cid2inchikey(cids):
    out = {}
    for c in cids:
        if c in over_air_cid2inchikey:
            out[c] = over_air_cid2inchikey[c]
            continue
        sleep(0.2)
        try:
            out[c] = Compound.from_cid(c).to_dict(properties=['inchikey'])['inchikey']
            over_air_cid2inchikey[c] = out[c]
        except:
            out[c] = 'no mapping'
    return out

def pubchem_inchikey2cid(d, ids):
    out = {}
    for c in ids:
        if c in d:
            continue
        sleep(0.2)
        try:
            r = get_compounds(str(c), 'inchikey')
            r = r.pop()
            r = r.to_dict(properties=['cid'])
            d[c] = r['cid']
        except:
            d[c] = 'no mapping'
    return d

def apply_mapping(ids, mapping):
    out = []
    
    for i in ids:
        try:
            a = mapping[i]
            if isinstance(a,set):
                a = a.pop()
        except:
            a = 'no mapping'
        out.append(a)
    
    return out

def concat_dict(dict1, dict2):
    out = defaultdict(set)
    for k in dict1:
        out[k].add(dict1[k])
    for k in dict2:
        out[k].add(dict2[k])
    return out

def count_missing(res, from_, to_, verbose = True):
    try:
        idx = [i for i,x in enumerate(res) if x == 'set()']
        for i in idx:
            res[i] = 'no mapping'
    except ValueError:
        pass
    
    l1 = len(res)
    l2 = res.count('no mapping')
    
    if l2 > 0 and verbose:
            warnings.warn(str(round(l2/l1,3))+' proportion of chemicals does not have mapping from '+from_ +' to '+to_)
        
    return l2/l1

def query_chembl(q, var, endpoint = None):
    q = """PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
    PREFIX owl: <http://www.w3.org/2002/07/owl#>
    PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
    PREFIX dc: <http://purl.org/dc/elements/1.1/>
    PREFIX dcterms: <http://purl.org/dc/terms/>
    PREFIX dbpedia2: <http://dbpedia.org/property/>
    PREFIX dbpedia: <http://dbpedia.org/>
    PREFIX foaf: <http://xmlns.com/foaf/0.1/>
    PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
    PREFIX cco: <http://rdf.ebi.ac.uk/terms/chembl#>
    """ + q
    endpoint = 'https://www.ebi.ac.uk/rdf/services/sparql'
    if endpoint: endpoint = endpoint
    res = query_endpoint(endpoint, q, var)
    sleep(0.2)
    return res

def query_mesh(q, var, endpoint = None):
    q = """PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
        PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
        PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
        PREFIX owl: <http://www.w3.org/2002/07/owl#>
        PREFIX meshv: <http://id.nlm.nih.gov/mesh/vocab#>
        PREFIX mesh: <http://id.nlm.nih.gov/mesh/>
        PREFIX mesh2019: <http://id.nlm.nih.gov/mesh/2019/>
        PREFIX mesh2018: <http://id.nlm.nih.gov/mesh/2018/>
        PREFIX mesh2017: <http://id.nlm.nih.gov/mesh/2017/>
    """ + q
    endpoint = 'http://id.nlm.nih.gov/mesh/sparql'
    if endpoint: endpoint = endpoint
    res = query_endpoint(endpoint, q, var)
    if len(var) < 2:
        res = [r[0] for r in res]
    sleep(0.2)
    return res

def tanimoto(compound1, compound2):
    fp1 = int(compound1.fingerprint, 16)
    fp2 = int(compound2.fingerprint, 16)
    fp1_count = bin(fp1).count('1')
    fp2_count = bin(fp2).count('1')
    both_count = bin(fp1 & fp2).count('1')
    return float(both_count) / (fp1_count + fp2_count - both_count)

class Chemistry:
    """
    Loading and manipulating chemistry data from various sources.
    """
    def __init__(self, cas_file = None, chemical_file = None, pubchem_directory = None, chemble_directory = None, endpoint = '', load_mappings = True, verbose = False, load_rdf=True):
        """
        args:
            cas_file :: str
                path to cas to inchikey mapping file.
            chemical_file :: str    
                path to chemical_id to inchikey mapping file.
            pubchem_directory :: str 
                optional, if loading from files.
            endpoint :: str 
                optional, host of endpoint with data.
                pubchem_directory or endpoint must be present
            cas_from_wikidata :: boolean
                optional, whether to load cas numbers from wikidata in addtion to mapping from cas_file.
            verbose :: boolean
                optional, whether to display warnings.
        """
        assert pubchem_directory or endpoint or not load_rdf
        
        self.endpoint = endpoint
        self.verbose = verbose
        
        if self.endpoint and not load_rdf:
            if not test_endpoint(endpoint):
                self.endpoint = ''
                if verbose: warnings.warn('Endpoint at:' + endpoint + ' not reached.')
        
        self.pubchem_directory = pubchem_directory
        self.chemble_directory = chemble_directory
        
        self.cas_file = cas_file
        self.chemical_file = chemical_file
        
        self.compound_namespace = Namespace('http://rdf.ncbi.nlm.nih.gov/pubchem/compound/')
        self.vocab_namespace = Namespace('http://rdf.ncbi.nlm.nih.gov/pubchem/vocabulary#')
        self.obo_namespace = Namespace('http://purl.obolibrary.org/obo/')
        self.mesh_namespace = Namespace('http://id.nlm.nih.gov/mesh/')
        
        self.initNs = {'compound':self.compound_namespace, 'vocab':self.vocab_namespace, 'obo': self.obo_namespace}
        
        if pubchem_directory and load_rdf:
            self.graph = Graph()
            if verbose: print('Loading hierarchy.')
            self._load_hierarchy()
            if verbose: print('Loading types.')
            self._load_type()
            for k,i in self.initNs.items():
                self.graph.bind(k,i)
        
        if chemble_directory and load_rdf:
            for f in os.listdir(chemble_directory):
                if f.endswith(".ttl"):
                    filename = os.path.join(chemble_directory, f)
                    self._load_data(filename)
        
        self._load_cas_mapping(load = load_mappings)
        self._load_mesh_mapping(load = load_mappings)
        self._load_cid_mapping(load = load_mappings)
        
        if self.chemical_file:
            self.chem2inchikey = chemical_id2inchikey(self.chemical_file)
        else:
            self.chem2inchikey = dict()
        
        self.load_rdf = load_rdf
        
    def __dict__(self):
        return {
                'namespace':self.namespace,
                'pubchem_directory': self.pubchem_directory,
                'endpoint': self.endpoint,
                'namespaces': self.initNs
            }
            
    def _apply_compound_namespace(self, cid):
        return self.compound_namespace['CID'+str(cid)]
    
    def _apply_mesh_namespace(self, cid):
        return self.mesh_namespace[str(cid)]
    
    def _load_cas_mapping(self, load = True, fname = 'inchikey_cas.csv'):
        if load:
            try:
                df = pd.read_csv(self.pubchem_directory+fname,sep=',', dtype = str)
                self.cas_mapping = {k:i for k,i in zip(df['inchikey'],df['id'])}
                
            except FileNotFoundError:
                self._load_cas_mapping(load = False)
        else:
            self.cas_mapping = {}
    
    def _load_mesh_mapping(self, load=False, fname = 'inchikey_mesh.csv'):
        if load and fname:
            try:
                df = pd.read_csv(self.pubchem_directory+fname,sep=',', dtype = str)
                self.mesh_mapping = {k:i for k,i in zip(df['inchikey'],df['id'])}
                
            except FileNotFoundError:
                self._load_mesh_mapping(load = False)
        else:
            self.mesh_mapping = {}
        
    def _load_cid_mapping(self, load=False, fname = 'inchikey_cid.csv'):
        if load and fname:
            try:
                df = pd.read_csv(self.pubchem_directory+fname,sep=',', dtype = str)
                self.cid_mapping = {k:i for k,i in zip(df['inchikey'],df['id'])}
                
            except FileNotFoundError:
                self._load_cid_mapping(load = False)
        else:
            self.cid_mapping = {}
    
    def _load_data(self, filename):
        g = Graph()
        g.parse(filename, format = 'ttl')
        return g
                
    def _load_hierarchy(self):
        self.graph += self._load_data(self.pubchem_directory + 'pc_compound2parent.ttl')
        
    def _load_type(self):
        self.graph += self._load_data(self.pubchem_directory + 'pc_compound_type.ttl')
    
    ### PRIVATE METHODS
    
    def _query(self, q, var):
        if self.endpoint:
            results = query_endpoint(self.endpoint, q, var = var)
        elif self.load_rdf:
            results = query_graph(self.graph, q)
        else:
            print('RDF data is not loaded. Set load_rdf = True.')
            results = []
        
        if len(var) < 2:
            results = [r[0] for r in results]
        
        return set(results)
    
    def _similarity(self, cids):
        compounds = []
        for c in cids:
            sleep(0.2)
            try:
                c = Compound.from_cid(c)
                compounds.append(c)
            except pubchempy.NotFoundError as e:
                print(c,e)
            except pubchempy.BadRequestError as e:
                print(c,e)
                
        if self.verbose:
            a = 1 - len(compounds)/len(cids)
            if a > 0:
                warnings.warn('{0} proportion was not found during similarity calculation'.format(a))
        out = []
        for c1 in compounds:
            for c2 in compounds:
                out.append({'compound1':c1.cid, 'compound2':c2.cid, 'similarity':tanimoto(c1,c2)})
        return out
    
    def _all_compounds(self):
        # only returns 10000 elements from endpoint
        q = prefixes(self.initNs)
        q += """
            SELECT ?s {
            ?s  ?o  ?z
            FILTER (isURI(?s) && STRSTARTS(str(?s), str(compound:) ) )
            }
            """
        results = self.query(q, var = ['s'])
        if len(results) == 10000:
            warnings.warn('Only 10k compounds will be loaded from endpoint.')
        
        out = set()
        for row in results:
            out.add(row)
        return out
    
    def _get_parent(self, cid):
        q1 = prefixes(self.initNs)
        q1 += """
        SELECT ?p WHERE{
            <%s> vocab:has_parent|rdf:type ?p 
        }
        """ % cid
        
        results = self._query(q1, var = ['p'])
        
        out = set()
        for row in results:
            out.add(row)
        return out
        
    def _get_child(self, cid):
        q1 = prefixes(self.initNs)
        q1 += """
        SELECT ?p WHERE {
            ?p vocab:has_parent|rdf:type <%s> 
        }
        """ % cid
        results = self._query(q1, var = ['p'])
        
        out = set()
        for row in results:
            out.add(row)
        return out
    
    def _get_sibling(self, cid):
        q = prefixes(self.initNs)
        q += """
        SELECT ?p WHERE {
            <%s> vocab:has_parent|rdf:type [
                ^vocab:has_parent|^rdf:type ?p
            ] .
        }
        """ % cid
        results = self._query(q, var = ['p'])
        
        out = set()
        for row in results:
            out.add(row)
        return out
        
    
    def _get_type(self, cid):
        q = prefixes(self.initNs)
        q += """
        SELECT ?p WHERE {
            <%s> rdf:type ?p 
        }
        """ % cid
        results = self._query(q, var = ['p'])
        
        out = set()
        for row in results:
            out.add(row)
        return out
    
    
    def _count_level(self, repeats, predicate):
        q = prefixes(self.initNs)
        predicate = '<' + predicate + '>'
        s = predicate
        for _ in range(repeats - 1):
            s += '/'
            s += predicate
            
        q += """
            ASK {[] %s []}
        """ % s
        
        if self.endpoint:
            sparql = SPARQLWrapper(self.endpoint)
            sparql.setQuery(q)
            sparql.setReturnFormat(JSON)
            results = sparql.query().convert()
            results = results['boolean']
        else:
            results = self.hierarcy.query(q)
        
        return results
    
    def _count_levels(self, predicate):
        i = 1
        while self._count_level(i, predicate):
            i += 1
        return i
    
    def _subset(self, cids, levels = 1, down = False):
        if levels == 0:
            return cids
        
        if levels == -1:
            levels = float('inf')
        
        if down:
            f = self._get_child
        else:
            f = self._get_parent
        
        out = set.union(*[set(f(c)) for c in cids])
        tmp = out
        while tmp and levels > 0:
            tmp = set.union(*[set(f(c)) for c in tmp])
            out |= tmp
            levels -= 1
            
        return out
    
    def _get_features(self, cids, params = None):
        out = {}
        for c in cids:
            sleep(0.2)
            try:
                if params:
                    out[c] = Compound.from_cid(c).to_dict(properties = params)
                else:
                    out[c] = Compound.from_cid(c).to_dict()
            except pubchempy.NotFoundError as e:
                print(c,e)
            except pubchempy.BadRequestError as e:
                print(c,e)
                
        return out
    
    def _save(self, pubchem_directory = './', f = FORMAT):
        if self.endpoint:
            raise NotImplementedError ('save from endpoint is not implemented.')
        else: 
            self.graph.serialize(pubchem_directory + 'chemistry.' + f , format = f)
    
    def _get_chembl_superclasses(self, uris):
        res = []
        for ur in chunks(uris, 10):
            s = ' '.join(['<'+u+'>' for u in ur])
            q = """ SELECT ?s ?c {
                    VALUES ?s { %s }
                    ?s rdfs:subClassOf | rdf:type ?c .
                    FILTER (!isBlank(?c)) .
                    FILTER (?s != ?c) .
                }
            """ % s
            e = None
            if self.endpoint: e = self.endpoint
            res.extend(query_chembl(q, var = ['s', 'c'], endpoint = e))
        return res
    
    def _get_mesh_superclasses(self, uris):
        res = []
        for ur in chunks(uris, 10):
            s = ' '.join(['<'+u+'>' for u in ur])
            q = """ SELECT ?s ?c {
                    VALUES ?s { %s }
                    ?s rdfs:subClassOf | rdf:type ?c .
                    FILTER (!isBlank(?c)) .
                    FILTER (?s != ?c) .
                }
            """ % s
            e = None
            if self.endpoint: e = self.endpoint
            res.extend(query_mesh(q, var = ['s', 'c'], endpoint = e))
        return res
    
    def _get_chembl_siblings(self, uris):
        res = []
        for ur in chunks(uris, 10):
            s = ' '.join(['<'+u+'>' for u in ur])
            q = """ SELECT ?s ?c {
                    VALUES ?s { %s }
                    ?s rdfs:subClassOf | rdf:type [ 
                    ^rdfs:subClassOf | ^rdf:type ?c 
                    ] .
                }
            """ % s
            e = None
            if self.endpoint: e = self.endpoint
            res.extend(query_chembl(q, var = ['s', 'c'], endpoint = e))
        return res
    
    def _get_subgraph(self, node, isLiteral = False):
        
        tmp = set([node])
        hist = set()
        out = set()
        
        while tmp:
            t = tmp.pop()
            hist.add(t)
            q = """
            SELECT ?s ?p ?o 
                {
                VALUES ?s {<%s>}
                ?s ?p ?o .
                FILTER (?s != ?o)
                FILTER(!isLiteral(?o))
                }
          
            """ % node
            tmp1 = self._query(q, var = ['s', 'p', 'o'])
            out |= tmp1
            tmp |= set([c for _,_,c in tmp1])
            
            q = """
            SELECT ?s ?p ?o 
                {
                VALUES ?o {<%s>}
                ?s ?p ?o .
                FILTER (?s != ?o)
                }
          
            """ % node
            
            tmp2 = self._query(q, var = ['s', 'p', 'o'])
            out |= tmp2
            tmp |= set([a for a,_,_ in tmp2])
            
            tmp -= hist
            
        return out
    
    def _get_mesh_subgraph(self, node):
        q = """SELECT DISTINCT ?p
            FROM <http://id.nlm.nih.gov/mesh>
            WHERE {
            ?s ?p ?o .
            } 
            """
        ps = query_mesh(q, var=['p'])
        
        tmp = set([node])
        hist = set()
        out = set()
        
        while tmp:
            t = tmp.pop()
            hist.add(t)
            for p in ps:
                q = """
                SELECT ?s ?p ?o 
                    {
                    VALUES ?s { <%s> }
                    VALUES ?p { <%s> }
                    ?s ?p ?o .
                    FILTER (?s != ?o)
                    FILTER(!isLiteral(?o))
                    }
            
                """ % (node, p)
                out |= self._query(q, var = ['s', 'p', 'o'])
                tmp |= set([c for _,_,c in out])
                
                q = """
                SELECT ?s ?p ?o 
                    {
                    VALUES ?o { <%s> }
                    VALUES ?p { <%s> }
                    ?s ?p ?o .
                    FILTER (?s != ?o)
                    }
            
                """ % (node, p)
                
                out |= self._query(q, var = ['s', 'p', 'o'])
                tmp |= set([a for a,_,_ in out])
            
            tmp -= hist
            
        return out
        
    ### PUBLIC FUNCTIONS
    
    def get_subgraph(self, cids, from_ = None, to_=None, use_mesh = False):
        if from_:
            if use_mesh:
                cids = self.convert_ids(from_, 'mesh', cids)
                cids = [self._apply_mesh_namespace(c) for c in cids if c != 'no mapping']
            else:
                cids = self.convert_ids(from_, 'cid', cids)
                cids = [self._apply_compound_namespace(c) for c in cids if c != 'no mapping']
        
        out = set()
        
        for node in tqdm(cids):
            if use_mesh:
                out |= self._get_mesh_subgraph(node)
            else:
                out |= self._get_subgraph(node)
            
        if to_:
            tt = 'cid'
            st = lambda x: strip(x, 'CID')
            if use_mesh: 
                tt = 'mesh'
                st = lambda x: strip(x, '/')
            
            items = [st(c) for c in cids]
            mapping = {a:m for a,m in zip(items, self.convert_ids(tt,to_,items)) if m != 'no mapping'}
            
            tmp = set()
            for a,b,c in out:
                tmpa = st(a)
                if tmpa in mapping:
                    a = mapping[tmpa]
                tmpb = st(b)
                if tmpb in mapping:
                    b = mapping[tmpb]
                tmp.add((a,b,c))
            out = tmp
            
        return out
    
    def similarity(self, cids, from_ = None, to_ = None):
        """
        Gets similarity of input ids.
        Args:
            cids :: list
                list of ids. Must be of type cid.
            from_ :: str
                optional, input type, inchikey, cas, chemical_id, or cid.
            to_ :: str
                optional, output type, inchikey, cas, chemical_id, or cid.
        Returns: 
            out :: list 
                dict:
                    key - compound1, compound2, similarity
                    item - cid, cid, float
        """
        
        def f(i, mapping):
            if isinstance(i, float):
                return i
            try:
                return mapping[str(i)]
            except KeyError:
                return 'no mapping'
        
        if from_: 
            tmp = self.convert_ids(from_, 'cid', cids)
            cids = [c for c in tmp if c != 'no mapping']
        out = self._similarity(cids)
        if to_:
            mapping = {k:i for k,i in zip(cids, self.convert_ids('cid', to_, cids))}
            out = [{k:f(d[k],mapping) for k in d} for d in out]
        return out
    
    
    def save(self, pubchem_directory = './', f = FORMAT):
        """
        Save data to RDF.
        Args:
            pubchem_directory :: str 
                path to save files at.
        """
        self._save(pubchem_directory, f)
    
    def get_parents(self, cids, from_ = None, to_ = None):
        """
        Gets parents of input ids.
        Args:
            cids :: list
                list of ids. Must be of type cid with appropriate rdf namespace. 
            from_ :: str
                optional, input type, inchikey, cas, chemical_id, or cid.
            to_ :: str
                optional, output type, inchikey, cas, chemical_id, or cid.
        Returns: 
            out :: dict 
                key - id
                item - list of parents of id
        """
        if from_: 
            cids = self.convert_ids(from_, 'cid', cids)
            cids = [c for c in cids if c != 'no mapping']
            cids = [self._apply_compound_namespace(c) for c in cids]
        
        out = {c:list(self._get_parent(c)) for c in cids}
        if to_:
            cids = set([strip(c,['#', '/', 'CID']) for c in cids])
            for k in out:
                cids |= set([strip(c,['#', '/', 'CID']) for c in out[k]])
            mapping = {k:i for k,i in zip(cids, self.convert_ids('cid', to_, cids))}
            out = {mapping[strip(k,['#', '/', 'CID'])]:[mapping[strip(a,['#', '/', 'CID'])] for a in out[k]] for k in out}
        return out
    
    def get_children(self, cids, from_ = None, to_ = None):
        """
        Gets children of input ids.
        Args:
            cids :: list
                list of ids. Must be of type cid with appropriate rdf namespace. 
            from_ :: str
                optional, input type, inchikey, cas, chemical_id, or cid.
            to_ :: str
                optional, output type, inchikey, cas, chemical_id, or cid.
        Returns: 
            out :: dict 
                key - id
                item - list of children of id
        """
        if from_: 
            cids = self.convert_ids(from_, 'cid', cids)
            cids = [c for c in cids if c != 'no mapping']
            cids = [self._apply_compound_namespace(c) for c in cids]
        
        out = {str(c):list(self._get_child(str(c))) for c in cids}
        if to_:
            cids = set([strip(c,['#', '/', 'CID']) for c in cids])
            for k in out:
                cids |= set([strip(c,['#', '/', 'CID']) for c in out[k]])
            mapping = {k:i for k,i in zip(cids, self.convert_ids('cid', to_, cids))}
            out = {mapping[strip(k,['#', '/', 'CID'])]:[mapping[strip(a,['#', '/', 'CID'])] for a in out[k]] for k in out}
        return out
    
    def get_siblings(self, cids, from_ = None, to_ = None):
        """
        Gets siblings of input ids. i.e. children of parent.
        Args:
            cids :: list
                list of ids. Must be of type cid with appropriate rdf namespace. 
            from_ :: str
                optional, input type, inchikey, cas, chemical_id, or cid.
            to_ :: str
                optional, output type, inchikey, cas, chemical_id, or cid.
        Returns: 
            out :: dict 
                key - id
                item - list of siblings of id
        """
        if from_: 
            cids = self.convert_ids(from_, 'cid', cids)
            cids = [c for c in cids if c != 'no mapping']
            cids = [self._apply_compound_namespace(c) for c in cids]
            
        out =  {c:self._get_sibling(c) for c in cids}
        if to_:
            cids = set([strip(c,['#', '/', 'CID']) for c in cids])
            for k in out:
                cids |= set([strip(c,['#', '/', 'CID']) for c in out[k]])
            mapping = {k:i for k,i in zip(cids, self.convert_ids('cid', to_, cids))}
            out = {mapping[strip(k,['#', '/', 'CID'])]:[mapping[strip(a,['#', '/', 'CID'])] for a in out[k]] for k in out}
        return out
        
        
    def get_types(self, cids, from_ = None, to_ = None):
        """
        Gets types (classes) of input ids.
        Args:
            cids :: list
                list of ids. Must be of type cid with appropriate rdf namespace. 
            from_ :: str
                optional, input type, inchikey, cas, chemical_id, or cid.
            to_ :: str
                optional, output type, inchikey, cas, chemical_id, or cid.
        Returns: 
            out :: dict 
                key - id
                item - list of types of id
        """
        def apply_mapping(a, mapping):
            try: 
                return mapping[a]
            except:
                return a
        
        if from_: 
            cids = self.convert_ids(from_, 'cid', cids)
            cids = [c for c in cids if c != 'no mapping']
            cids = [self._apply_compound_namespace(c) for c in cids]
        
        out =  {c:self._get_type(c) for c in cids}
        if to_:
            cids = [strip(c,['#', '/', 'CID']) for c in cids]
            mapping = {k:i for k,i in zip(cids, self.convert_ids('cid', to_, cids))}
            out = {apply_mapping(k,mapping):set([apply_mapping(a,mapping) for a in out[k]]) for k in out}
        return out
        
    def get_chembl_superclasses(self, uris):
        res = self._get_chembl_superclasses(uris)
        out = defaultdict(set)
        for s,c in res:
            out[s].add(c)
        return out
    
    def get_mesh_superclasses(self, uris):
        res = self._get_mesh_superclasses(uris)
        out = defaultdict(set)
        for s,c in res:
            out[s].add(c)
        return out
    
        
    def subset(self, cids, up_levels = 1, down_levels = 1, from_ = None, to_ = None):
        """
        Traverses tree up/down and returns all entities passed.
        Args:
            cids :: list
                list of ids. Must be of type cid with appropriate rdf namespace. 
            up_levels :: int
                number of levels to traverse tree upward. -1 denotes to the top.
            down_levels :: int
                number of levels to traverse tree downward. -1 denotes to the bottom.
            from_ :: str
                optional, input type, inchikey, cas, chemical_id, or cid.
            to_ :: str
                optional, output type, inchikey, cas, chemical_id, or cid.
        Returns: 
            out :: list 
                all entities passed while traversing.
        """
        
        if from_: 
            cids = self.convert_ids(from_, 'cid', cids)
            cids = [c for c in cids if c != 'no mapping']
            cids = [self._apply_compound_namespace(c) for c in cids]
        
        cids = set(cids)
        
        tmp1 = self._subset(cids, up_levels, down = False)
        tmp2 = self._subset(cids, down_levels, down = True)
        tmp = cids | tmp1 | tmp2
        for k,i in self.get_siblings(tmp1).items():
            tmp |= i
        for k,i in self.get_siblings(tmp2).items():
            tmp |= i
        
        if to_:
            tmp = [strip(c, ['CID']) for c in tmp]
            tmp = self.convert_ids('cid', to_, list(tmp))
            
        return tmp
    
    def compounds(self):
        """
        Returns all compounts.
        Returns:
            list
        """
        return self._all_compounds()
    
    def construct_tree(self, subset = None, from_ = None, to_ = None):
        """
        Traverses tree up and down and returns all entities passed.
        Args:
            cids :: list
                list of ids. Must be of type cid with appropriate rdf namespace. 
            up_levels :: int
                number of levels to traverse tree upward. -1 denotes to the top.
            down_levels :: int
                number of levels to traverse tree downward. -1 denotes to the bottom.
            from_ :: str
                optional, input type, inchikey, cas, chemical_id, or cid.
            to_ :: str
                optional, output type, inchikey, cas, chemical_id, or cid.
        Returns: 
            out :: list 
                all entities passed while traversing.
        """
        if from_: 
            subset = self.convert_ids(from_, 'cid', subset)
            subset = [c for c in subset if c != 'no mapping']
            subset = [self._apply_compound_namespace(c) for c in subset]
            
        if not subset:
            subset = self._all_compounds()
        
        out = []
        all_keys = set()
        for k,i in self.get_parents(subset).items():
            for a in i:
                out.append({'child':k, 'parent':a, 'score':1})
                all_keys.add(strip(a,'CID'))
                all_keys.add(strip(k,'CID'))
            
        for k,i in self.get_children(subset).items():
            for a in i:
                out.append({'child':a, 'parent':k, 'score':1})
                all_keys.add(strip(a,'CID'))
                all_keys.add(strip(k,'CID'))
        
   
        def f(i, mapping):
            if isinstance(i, int):
                return i
            return mapping[i]
        
        if to_:
            mapping = {k:i for k,i in zip(list(all_keys), self.convert_ids('cid', to_, list(all_keys)))}
            out = [{k:f(strip(d[k],'CID'), mapping) for k in d} for d in out]
            
        return out
    
    def query(self, q, var):
        """
        Custom query of the data.
        Args:
            q :: str 
                query, without namespace prefixes, self.prefixes_used() will provide namespaces.
            var :: list
                list of variables to extract from results.
        Return:
            list of tuples (of len(var))
        """
        q = prefixes(self.initNs) + q
        return self._query(q, var = var)
    
    def prefixes_used(self):
        return self.initNs
    
    def get_features(self, cids, from_ = None, to_ = None, params = None):
        """
        Get features for chemicals.  
        Args:
            cids :: list
                list of ids. Must be of type cid with appropriate rdf namespace. 
            from_ :: str
                optional, input type, inchikey, cas, chemical_id, or cid.
            to_ :: str
                optional, output type, inchikey, cas, chemical_id, or cid.
        Returns: 
            out :: dict
                key - id
                item - dict 
                    key - feature 
                    item - value
        """
        if from_: 
            cids = self.convert_ids(from_,'cid', cids)
        
        out = self._get_features(cids, params)
        
        if to_:
            f = {k:i for k,i in zip(list(out.keys()), self.convert_ids('cid', to_, list(out.keys())))}
            out = {f[k]:out[k] for k in out}
        
        return out
    
    def class_hierarchy(self, cids, from_ = None, to_ = None, use_mesh = False):
        """
        Construct class hierarchy on adjecency form.
        Args:
            cids :: list
                list of ids. Must be of type cid with appropriate rdf namespace. 
            from_ :: str
                optional, input type, inchikey, cas, chemical_id, or cid.
            to_ :: str
                optional, output type, inchikey, cas, chemical_id, or cid.
        Returns:
            out :: list
                list of child, parent, score pairs
        """
        if from_:
            if use_mesh:
                cids = self.convert_ids(from_, 'mesh', cids)
                cids = [self._apply_mesh_namespace(c) for c in cids if c != 'no mapping']
            else:
                cids = self.convert_ids(from_, 'cid', cids)
                cids = [self._apply_compound_namespace(c) for c in cids if c != 'no mapping']
        
        f = self.get_chembl_superclasses
        if use_mesh == True:
            f = self.get_mesh_superclasses
        
        out = set()
        if use_mesh:
            tmp = set(cids)
        else:
            types = self.get_types(cids)
            for k in types:
                for i in types[k]:
                    out.add((URIRef(k), URIRef(i), 1))
            
            tmp = set([p for _,p,_ in out])
            
        ids = list(tmp)
        for i in tqdm(ids):
            tmp = set([i])
            hist = set()
            while tmp:
                if len(tmp) < 1: break
                classes = f(tmp)
                for k in classes:
                    for i in classes[k]:
                        tmp.add(URIRef(i))
                        out.add((URIRef(k), URIRef(i), 1))
                        hist.add(URIRef(k))
                tmp -= hist
                if all([not classes[k] for k in classes]):
                    break
        
        if to_:
            tt = 'cid'
            st = lambda x: strip(x, 'CID')
            if use_mesh: 
                tt = 'mesh'
                st = lambda x: strip(x, '/')
            
            items = [st(c) for c in cids]
            mapping = {a:m for a,m in zip(items, self.convert_ids(tt,to_,items)) if m != 'no mapping'}
            
            tmp = set()
            for a,b,c in out:
                tmpa = st(a)
                if tmpa in mapping:
                    a = mapping[tmpa]
                tmpb = st(b)
                if tmpb in mapping:
                    b = mapping[tmpb]
                tmp.add((a,b,c))
            out = tmp
                
        return out
    
    def get_names(self, cids, from_ = None, to_ = None):
        if from_:
            cids = self.convert_ids(from_, 'cid', cids)
        
        out = {}
        for c in cids:
            try:
                out[c] = []
                tmp = Compound.from_cid(c)
                out[c] = tmp.synonyms
            except pubchempy.BadRequestError as e:
                print(c,e)
            except pubchempy.NotFoundError as e:
                print(c,e)
                
        if to_:
            f = {k:i for k,i in zip(list(out.keys()), self.convert_ids('cid', to_, list(out.keys())))}
            out = {f[k]:out[k] for k in out}
            
        return out
    
    def get_fingerprint(self, cids, from_ = None, to_ = None):
        if from_:
            cids = self.convert_ids(from_, 'cid', cids)
        
        out = {}
        for c in cids:
            try:
                tmp = Compound.from_cid(c)
                out[c] = tmp.cactvs_fingerprint
            except pubchempy.BadRequestError as e:
                print(c,e)
            except pubchempy.NotFoundError as e:
                print(c,e)
                
        if to_:
            f = {k:i for k,i in zip(list(out.keys()), self.convert_ids('cid', to_, list(out.keys())))}
            out = {f[k]:out[k] for k in out}
        
    def convert_ids(self, from_, to_, ids):
        """
        Convert between types of ids used in chemical data.
        
        Args:
            from_ :: str 
                input id type, inchikey, cas, chemical_id or cid.
            to_ :: str 
                output id type, inchikey, cas, chemical_id or cid.
            ids :: list
                list of ids, 'no mapping' if no mapping between from_ and to_ exists.
        Return:
            ids :: list
                converted ids.
        Raises:
            NotImplementedError: if from_ or to_ not in (inchikey, cas, chemical_id, cid)
        """
        ids = [str(i) for i in ids]
        l1 = len(ids)
        
        if len(ids) < 1:
            return ids
        
        if from_ == to_:
            return ids
        
        if not from_ in ['cas','inchikey','cid','mesh','chemical_id'] or not to_ in ['cas','inchikey','cid','mesh','chemical_id']:
            raise NotImplementedError (from_+' to ' + to_ + ' is not supported')
        
        if from_ == 'inchikey':
            if to_ == 'cas':
                mapping = inchikey2cas(self.cas_mapping, ids)
            elif to_ == 'chemical_id':
                mapping = reverse_mapping(self.chem2inchikey)
            elif to_ == 'cid':
                mapping = inchikey2cid(self.cid_mapping, ids)
            elif to_ == 'mesh':
                mapping = inchikey2mesh(self.mesh_mapping, ids)
            ids = apply_mapping(ids, mapping)
            
        elif from_ == 'cas':
            mapping = inchikey2cas(self.cas_mapping, ids)
            mapping = reverse_mapping(mapping)
            ids = apply_mapping(ids, mapping)
            if to_ == 'chemical_id':
                return self.convert_ids('inchikey', 'chemical_id', ids)
            elif to_ == 'cid':
                return self.convert_ids('inchikey', 'cid', ids)
            elif to_ == 'mesh':
                return self.convert_ids('inchikey', 'mesh', ids)
            
        elif from_ == 'chemical_id':
            mapping = self.chem2inchikey
            ids = apply_mapping(ids, mapping)
            if to_ == 'cas':
                return self.convert_ids('inchikey', 'cas', ids)
            elif to_ == 'cid':
                return self.convert_ids('inchikey', 'cid', ids)
            elif to_ == 'mesh':
                return self.convert_ids('inchikey', 'mesh', ids)
        
        elif from_ == 'cid':
            mapping = inchikey2cid(self.cid_mapping, ids)
            mapping = reverse_mapping(mapping)
            ids = apply_mapping(ids, mapping)
            if to_ == 'cas':
                return self.convert_ids('inchikey', 'cas', ids)
            elif to_ == 'chemical_id':
                return self.convert_ids('inchikey', 'chemical_id', ids)
            elif to_ == 'mesh':
                return self.convert_ids('inchikey', 'mesh', ids)
        
        elif from_ == 'mesh':
            mapping = inchikey2mesh(self.mesh_mapping, ids)
            mapping = reverse_mapping(mapping)
            ids = apply_mapping(ids, mapping)
            if to_ == 'cas':
                return self.convert_ids('inchikey', 'cas', ids)
            elif to_ == 'cid':
                return self.convert_ids('inchikey', 'cid', ids)
            elif to_ == 'chemical_id':
                return self.convert_ids('inchikey', 'chemical_id', ids)
        
        res = [str(s) for s in ids]
        
        if self.verbose: count_missing(res, from_, to_)
        
        return res
        
                
                
