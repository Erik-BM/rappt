#!/usr/bin/env python

from distutils.core import setup 

setup(name='rappt',
      version='1.0',
      description='Risk Assessment Pre-Processing Tools',
      author='Erik Bryhn Myklebust',
      author_email='ebm@niva.no',
      url='gitlab.com/Erik-BM/rappt',
      packages=['rappt'],
     )
