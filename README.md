# DEPRECIATED
Version 0.2.0 is available [here](https://github.com/Erik-BM/TERA) .

# Processing tools for data used in ecological risk assessment. 
If you use these APIs in your work, please cite: [**TERA: the Toxicological Effect and Risk Assessment Knowledge Graph**](https://arxiv.org/abs/1908.10128). 

## Purpose
Integrate ECOTOX data with commonly used taxonomies and chemical data using semantic web technologies. 

## Data sources
* [ECOTOX](https://cfpub.epa.gov/ecotox/)
* [NCBI Taxonomy](https://www.ncbi.nlm.nih.gov/taxonomy)
* [PubChem](https://pubchem.ncbi.nlm.nih.gov)
* [ChEMBL](https://www.ebi.ac.uk/chembl/)
* Encyclopedia of Life ([EOL](https://www.eol.org))
* [Wikidata](https://www.wikidata.org/wiki/Wikidata:Main_Page)

## Install
```
python3 setup.py install
```

## Files
### download_data.sh
Downloads and converts all public data needed. Run by `bash download_data.sh`. May need to be run as super user. This will create the directories `taxdump`, `eol`, `pubchem`, and `ecotox_data`.

### rappt/taxonomy.py
The `Taxonomy` class takes either a sparql endpoint or a file directory (`taxdump`) as input. In addition, the class takes an namespace as input, defaults to http://www.example.org/ncbi/ . When loading from files, `Taxonomy` have a `save` method that allows to save the data to RDF files. The method `species` returns all species in the dataset. `names` returns all synonyms for all or a optional subset of the species in the dataset. `construct_tree` returns `child, parent` pairs of all or a optinal subset of species. The `subset` method takes a set of species, a number of up levels, and down levels. This method returns all taxa within taxonomy levels given of the input set of species. 

### rappt/ecotox.py
The `Ecotox` also take either an endpoint or directory (`ecotox_RELEASE_DATE`). In addition to methods avalible for `Taxonomy`, `Ecotox` has the method `chemicals`, which returns all chemicals in the dataset. `Ecotox` does not have the `subset` method.

### rappt/chemicals.py
The `Chemicals` class makes access to `PubChem` and `Mesh` easy. Takes a set of chemicals as inputs along with chemical identifier as input (`inchikey, cas, chemical_id`). If input is not in `inchikey`, one must provide additional mapping files. Final input is a optional endpoint.

### rappt/alignment.py
The Alignment class takes the output RDF file from [LogMap](https://github.com/ernestojimenezruiz/logmap-matcher/tree/master/src/main/java/uk/ac/ox/krr/logmap2) and converts it to mapping dictonaries. Note, to use the alignment class, one must first save the species trees from both `taxonomy.py` and `ecotox.py` and align them using LogMap. One could also use an alignment method implemented in `ecotox.py`, which stricly map using taxon names. 

### rappt/keys.py
Conversion factors, data types definitions etc.

### rappt/utils.py
Utilities.

## Examples
Examples are found [here](https://gitlab.com/Erik-BM/rappt/tree/master/examples).

## Using triple store (Persistent storage)
Install [docker](https://docs.docker.com/install/).
First `mkdir rdf`. Place RDF file here. (use the `save(directory='./rdf/')` method on Taxonomy, Ecotox and Chemistry to generate them). 

See more about the tenforce/virtuoso docker image [here](https://hub.docker.com/r/tenforce/virtuoso).

First, pull container
```
docker pull tenforce/virtuoso
```

Run docker container with (may require super user from here on):
```
docker run --name my-virtuoso \
    -p 8890:8890 -p 1111:1111 \
    -e DBA_PASSWORD=password \
    -e SPARQL_UPDATE=true \
    -e DEFAULT_GRAPH=http://www.example.com/my-graph \
    -v /tmp/virtuoso/data:/data \
    -d tenforce/virtuoso
```
Then make a data folder
```
mkdir -p /tmp/virtuoso/data/toLoad
```
Move RDF-files into the data folder.
```
cp ./rdf/* /tmp/virtuoso/data/toLoad
```
```
docker exec -it my-virtuoso bash
isql-v -U dba -P $DBA_PASSWORD
ld_dir('toLoad', '*.nt', 'http://www.example.com/my-graph');
rdf_loader_run();
```
Verify that the triples were loaded:
```
select * from DB.DBA.load_list;
```

Also available at http://localhost:8890/sparql. 

If rdf files are changed, run:
```
DELETE FROM DB.DBA.RDF_QUAD ;
DELETE FROM DB.DBA.load_list;
```
before `ld_dir` method.


## Related publications

-  Erik B. Myklebust, Ernesto Jimenez-Ruiz, Zofia C. Rudjord, Raoul Wolf, Knut Erik Tollefsen: [**Integrating  semantic  technologies  in  environmental  risk  assessment:  A  vision**](https://s3.amazonaws.com/setac.mms.uploads/m_48/extended_abstracts/49766_Myklebust/EBMyklebust_et_al_Semantics_and_risk_assessment.pdf).  29th Annual Meeting of the Society of Environmental Toxicology and Chemistry ([SETAC](https://helsinki.setac.org/)) (2019)

- Erik B. Myklebust, Ernesto Jimenez-Ruiz, Jiaoyan Chen, Raoul Wolf, Knut Erik Tollefsen. [**Knowledge Graph Embedding for Ecotoxicological Effect Prediction**](https://arxiv.org/abs/1907.01328). International Semantic Web Conference 2019. *Best Student Paper in the In-Use track of ISWC 2019*

- Erik B. Myklebust, Ernesto Jimenez-Ruiz, Jiaoyan Chen, Raoul Wolf, Knut Erik Tollefsen. [**TERA: the Toxicological Effect and Risk Assessment Knowledge Graph**](https://arxiv.org/abs/1908.10128). Submitted to a conference, 2019. 













