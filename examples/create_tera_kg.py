#example use of all APIs to create the TERA KG. 

from rappt import Taxonomy
from rappt import Chemistry
from rappt import Ecotox
from rappt import Alignment

from rappt import strip

from rdflib import Graph, URIRef, Literal
from rdflib.namespace import OWL, RDFS

basepath = '../'
a = Alignment('./data/logmap2_mappings.rdf')

e = Ecotox(directory = basepath+'ecotox_data/', namespace = 'http://example.org/ecotox', verbose = True)
e.save(basepath+'rdf/')

t = Taxonomy(directory = basepath+'taxdump/', eol_directory=basepath+'eol/', namespace = 'http://example.org/ncbi', verbose = True)
t.save(basepath+'rdf/')

for s,p,o in t.graph:
    if isinstance(o, Literal):
        t.graph.remove( (s,p,o) )
t.graph.serialize(basepath+'rdf/taxonomy_no_literals.nt',format='nt')

c = Chemistry(pubchem_directory = basepath+'pubchem/', verbose = True)
c.save(basepath+'rdf/')

### Mapping CAS to CID
chems = [strip(s,['/','#']) for s in e.chemicals()]
cids = c.convert_ids(from_='cas',to_='cid',ids=chems)

sameas_graph = Graph()

d = a.ecotox2ncbi_mapping()
for k in d:
    a,b = URIRef(k),URIRef(d[k])
    sameas_graph.add((a,OWL.sameAs,b))
    
for a,b in zip(chems, cids):
    if a and b:
        a = e.namespace['chemical/'+str(a)]
        b = c.compound_namespace['CID'+str(s)]
        a,b = URIRef(a), URIRef(b)
        sameas_graph.add((a,OWL.sameAs,b))
        
sameas_graph.serialize(basepath+'rdf/equiv.nt',format='nt')
