# visualize Copper and Zinc sulfate data for 10 most used species

from rappt import Ecotox

from collections import defaultdict
import pandas as pd

from scipy import stats
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
import sys
from decimal import Decimal

from sklearn.preprocessing import RobustScaler

e = Ecotox('../../ecotox_data',endpoints_to_consider=['LC50','EC50'],effects_to_consider=['MOR'],verbose=False)

sulfates = ['7758-98-7', '7733-02-0']
sulfates = [s.replace('-','') for s in sulfates]
sulfates_labels = ['Copper Sulfate','Zinc Sulfate']
compound_names = {n:nn for n,nn in zip(sulfates,sulfates_labels)}

endpoints = e.endpoints(chems=sulfates, species=None, convert=True)

species_names = e.species_names(convert=True)
for k in species_names:
    try:
        species_names[k] = species_names[k].pop(0)
    except:
        species_names[k] = k
        
epsilon = sys.float_info.epsilon

data = []
for d in endpoints:
    cas,sid,c,u = d['cas'],d['taxon'],d['concentration'],d['concentration_unit']
    
    c = e.unit_convertion(c,u)
    
    if c < 0: continue
    data.append((cas,sid,c))

dist_count = defaultdict(int)
count_c = defaultdict(int)
count_s = defaultdict(int)

for c,s,conc in data:
    dist_count[(c,s)] += 1
    count_c[c] += 1
    count_s[s] += 1

cs = sulfates

for s in count_s:
    a = 0
    for c in cs:
        a += dist_count[(c,s)]
    count_s[s] = a
ss = sorted(count_s.keys(), key=lambda x:count_s[x],reverse=True)
ss = ss[:10] # 10 species

dist = defaultdict(list)
for c,s,conc in data:
    if not c in cs or not s in ss: continue
    dist[(c,s)].append(conc)

dataframe = defaultdict(list)
for k in dist:
    c,s = k
    for v in dist[k]:
        dataframe['concentration'].append(v)
        a = np.asarray(dist[k])
        dataframe['mean_concentration'].append(np.mean(a))
        dataframe['median_concentration'].append(np.median(a))
        dataframe['var_concentration'].append(np.var(a))
        
        v = np.log10(v + epsilon)
        dataframe['log_concentration'].append(v)
        
        a = np.log10(dist[k] + np.repeat(epsilon,len(dist[k])))
        try:
            dataframe['mean_log_concentration'].append(np.mean(a))
        except:
            dataframe['mean_log_concentration'].append(np.nan)
        try:
            dataframe['median_log_concentration'].append(np.median(a))
        except:
            dataframe['median_log_concentration'].append(np.nan)
        try:
            dataframe['var_log_concentration'].append(np.var(a))
        except:
            dataframe['var_log_concentration'].append(np.nan)
        
        dataframe['species'].append(s)
        dataframe['species_name'].append(species_names[s])
        dataframe['compound'].append(c)
        dataframe['compound_name'].append(compound_names[c])

dataframe = pd.DataFrame.from_dict(dataframe)

ax = sns.violinplot(x='compound_name',y='log_concentration',hue='species_name',data=dataframe)

ax.tick_params(axis='both', which='major', labelsize=20)
ax.tick_params(axis='both', which='minor', labelsize=16)

y = np.arange(-18,0,1,dtype=float)
plt.yticks(y,['%.1e' % Decimal(str(10**c)) for c in y])
plt.legend(loc='lower right', prop={'size': 12})
plt.xlabel('Compound',fontsize=32)
plt.ylabel('Concentration (mg/L)',fontsize=32)
plt.show()

